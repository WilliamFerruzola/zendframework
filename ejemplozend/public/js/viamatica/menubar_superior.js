    /*------------------------------------------------------------------------------------------------------------------*/
    /*
     * Permite activar los menus tipo office 2007
     * 
     * Los eventos que aceptan los botones son los siguientes:
     * 
     *     Panel            Frame           Boton
     *     =============    =============== =================
     *     div_principal    opciones        nuevo
     *                                      grabar
     *                                      eliminar
     *                                      salir
     *                                      
     *                      exportar        excel    
     *                                      pdf
     *                                      xml
     *                                    
     *                      documentos      nuevo
     *                                      listado
     *                                      
     *                      logaccessos     listado
     *                      
     *    personalizar      opcion          editar                  
     **/
    
    function MenuBar_Button_EventClickUp(panel, frame, boton, funcion){
        if($('#li_'+panel).length==0)
            { alert('MenuBar_EnableButton:: flange '+'li_'+panel+' undefined');return; }
        //Existencia del frame
        if($('#li_'+panel+'_'+frame).length==0)
            { alert('MenuBar_EnableButton:: frame '+'li_'+panel+'_'+frame+' undefined');return; }
        //Existencia del boton encendido
        if($('#div_'+panel+'_'+frame+'_'+boton).length==0)
            { alert('MenuBar_EnableButton:: button '+'div_'+panel+'_'+frame+'_'+boton+' undefined');return; }
        //Existencia del boton apagado
        if($('#div_'+panel+'_'+frame+'_'+boton+'_apagado').length==0)
            { alert('MenuBar_EnableButtonApagado:: button '+'div_'+panel+'_'+frame+'_'+boton+'_apagado'+' undefined');return; }
        //Existencia del Hidden del boton
        if($('#link_'+panel+'_'+frame+'_'+boton).length==0)
            { alert('MenuBar_EnableButton:: button '+'link_'+panel+'_'+frame+'_'+boton+' undefined');return; }


        $("#div_"+panel+"_"+frame+"_"+boton).unbind();
        $("#div_"+panel+"_"+frame+"_"+boton).mouseup(funcion);
    }//end MenuBar_Button_EventClick
    

    /*------------------------------------------------------------------------------------------------------------------*/
    //Permite escoger la pestana inicial de la opcion
    /*------------------------------------------------------------------------------------------------------------------*/
    function PestanaInicial(nombre){ 
        //***************************************************************************************************************************************************
        if(nombre!='principal'){
        $("#li_principal").removeClass("current");
        }
        //***************************************************************************************************************************************************
        if(nombre!='personalizar'){
        $("#li_personalizar").removeClass("current");
        }	
        //***************************************************************************************************************************************************				
    } //end function PestanaInicial


    /*------------------------------------------------------------------------------------------------------------------*/
    //Permite elegir cual de los controles presentar y cuales no	
    /*------------------------------------------------------------------------------------------------------------------*/
    function MenuBar_EnableButton(flange_control,frame_control,btn_control,status){ 
        //Existencia de la pestana 
        if($('#li_'+flange_control).length==0)
            { alert('MenuBar_EnableButton:: flange '+'li_'+flange_control+' undefined');return; }
        //Existencia del frame
        if($('#li_'+flange_control+'_'+frame_control).length==0)
            { alert('MenuBar_EnableButton:: frame '+'li_'+flange_control+'_'+frame_control+' undefined');return; }
        //Existencia del boton encendido
        if($('#div_'+flange_control+'_'+frame_control+'_'+btn_control).length==0)
            { alert('MenuBar_EnableButton:: button '+'div_'+flange_control+'_'+frame_control+'_'+btn_control+' undefined');return; }
        //Existencia del boton apagado
        if($('#div_'+flange_control+'_'+frame_control+'_'+btn_control+'_apagado').length==0)
            { alert('MenuBar_EnableButton:: button '+'div_'+flange_control+'_'+frame_control+'_'+btn_control+'_apagado'+' undefined');return; }
        //Existencia del Hidden del boton
        if($('#link_'+flange_control+'_'+frame_control+'_'+btn_control).length==0)
            { alert('MenuBar_EnableButton:: button '+'link_'+flange_control+'_'+frame_control+'_'+btn_control+' undefined');return; }

        //Presenta las pestanas,frames y botones
        $('#li_'+flange_control).show(0);
        $('#li_'+flange_control+'_'+frame_control).show(0);

        if(status){
                $('#div_'+flange_control+'_'+frame_control+'_'+btn_control+'_apagado').hide(0);
                $('#div_'+flange_control+'_'+frame_control+'_'+btn_control).show(0);
        }else{
                $('#div_'+flange_control+'_'+frame_control+'_'+btn_control).hide(0);
                $('#div_'+flange_control+'_'+frame_control+'_'+btn_control+'_apagado').show(0);
        }
    /*------------------------------------------------------------------------------------------------------------------*/		
    } //end function visible_buttons
    /*------------------------------------------------------------------------------------------------------------------*/

    function MenuBar_EnableButtons(habilitar){
        if (habilitar){
            $(".menubarSuperior_boton_activo").show(0);
            $(".menubarSuperior_boton_inactivo").hide(0);
        }else{
            $(".menubarSuperior_boton_activo").hide(0);
            $(".menubarSuperior_boton_inactivo").show(0);
        }
    }//end function
    
    /*
     * Permite carga en el div_menu_bar en el Layout 
     * el contenido de la barra de herramientas
     */
    function MenuBar_Cargar(contenido){
        $("#div_menu_bar").html(contenido);   
    }//end function MenuBar_Cargar
    
    