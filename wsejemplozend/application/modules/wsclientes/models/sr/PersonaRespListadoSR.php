<?php
require_once('PersonaSR.php');

class PersonaRespListadoSR {
    /** @var PersonaSR[] */
    public $registros;
    /** @var integer */
    public $tot_registros           = null;
    /** @var integer */
    public $tot_paginas             = null;
    /** @var String */
    public $error_codigo            = null;
    /** @var String */
    public $error_mensaje           = null;
     /** @var Array */
    public $exception               = null;    
}

?>