<?php
error_reporting(E_ALL);

require_once 'wsclientes/models/bo/PersonaBO.php'; 
 
class Wsclientes_PersonaController extends Zend_Controller_Action{
 	private $_WSDL_URI;
	private $_config;

    public function init(){
		//Se inicializa las variables del entorno
		$this->_WSDL_URI    = Viamatica_BaseWWW::getBaseWWW().$this->getRequest()->getBaseUrl()."/wsclientes/persona?wsdl";
		
		$this->_config 	    = Zend_Registry::get('config');
	}//end function init()
 /*---------------------------------------------------------------------------------------*/	
       
        
    public function indexAction(){
	   $this->_helper->viewRenderer->setNoRender(true); //Desactiva la Vista

            if(isset($_GET['wsdl'])) {
                    try{  	
                        $autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
                        $autodiscover->setOperationBodyStyle(
                         array('use' => $this->_config->ws->use,
                               'namespace' =>  $this->_config->ws->namespace)
                         );
                        $autodiscover->setClass('PersonaBO');
                        $autodiscover->handle();
                    }catch (Zend_Exception $e) {  
                        echo "Error message (hadleWSDL): " . $e->getMessage() . "\n";  
                    }
            } else {
                    try{  	
                        $soap = new Zend_Soap_Server($this->_WSDL_URI, array('encoding' => $this->_config->ws->encoding, 'soap_version' => SOAP_1_1)); 
                        $soap->setClass('PersonaBO');
                        $soap->setObject(new PersonaBO());
                        $soap->setEncoding($this->_config->ws->encoding);
                        $soap->handle();
                    }catch (Zend_Exception $e) {  
                        echo "Error message (handleSOAP): " . $e->getMessage() . "\n";  
                    }  			        
            }//end if
	}//end function indexAction
/*---------------------------------------------------------------------------------------*/


	
    public function clientAction() {
 
        $this->_helper->viewRenderer->setNoRender(true); //Desactiva la Vista
            try{  	
                $client = new Zend_Soap_Client($this->_WSDL_URI,array('encoding'=>'ISO-8859-1'));  //ISO-8859-1
                //$client = new PersonaBO();  

                $result = $client->listado("cod_persona", "asc",1 , 5 , '', '');
               
                var_dump($result);

            }catch (Zend_Exception $e) { 
                     var_dump($e->getMessage());
                     var_dump($e->getTrace());
            }  			
    }//end function function clientAction()

}//end class Wscliente_PersonaController 