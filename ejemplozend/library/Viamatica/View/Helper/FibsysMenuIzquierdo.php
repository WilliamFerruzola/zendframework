<?php
require_once 'seguridad/models/segPermiso.php';

class Zend_View_Helper_FibsysMenuIzquierdo extends Zend_View_Helper_Abstract{

    public function FibsysMenuIzquierdo()
    {
        $session_usuario = new Zend_Session_Namespace('usuario');
        
        //Ubico la ruta base utilizando el Helper de la Vista
        $helper_view = new Zend_View_Helper_BaseUrl();
        $UrlBase = $helper_view->baseUrl();
        
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            return false;
        }//end if

        $filas = Zend_Auth::getInstance()->getIdentity()->accesos;        
        
        
//        $session_usuario = new Zend_Session_Namespace('usuario');//instancia de la session del usuario
//        $filas=$session_usuario->accesos;	//extraccion de los accesos de la session del usuario	

        $html="<table width='100%'  border='0' cellspacing='0' cellpadding='0'>";
        $html.="<tr>";
        $modulo = '';
        $i=0;


        /*-----Obtiene el modulo de Apertura----*/
        if ($session_usuario->modulo_apertura){
            $CodModuloApertura = $session_usuario->modulo_apertura;
            $SegPermiso = new SegPermiso();        
            foreach ($filas as $r){
                $SegPermiso = $r;
                if($session_usuario->modulo_apertura == $SegPermiso->getCod_modulo()){
                     $NomModuloApertura = $SegPermiso->getNom_modulo();
                     break;
                }//end if
            }//end foreach
        }else{
            $SegPermiso = new SegPermiso();  
            $SegPermiso = $filas[0];
            $CodModuloApertura = $SegPermiso->getCod_modulo();
            $NomModuloApertura = $SegPermiso->getNom_modulo();
        }//end if
        /*-------------------------------------*/
        
        
        $html.=" <td  class='gris12bold' style='background:url(".$UrlBase."/images/general/bg_tittle_menu.jpg); background-repeat:repeat-x; padding-left:5px; height:30px;'><p>::".$NomModuloApertura."</p></td>";
        $html.=" </tr>";
        $html.="</table>";

        $html.="<table width='155' border='0' cellspacing='0' cellpadding='0'>";

        $SegPermiso = new SegPermiso();        
        foreach ($filas as $r) 
        {
            $SegPermiso = $r;
            if($CodModuloApertura == $SegPermiso->getCod_modulo())
            {
                $html.="<tr>";
                $html.="<td align='left' valign='middle'>&nbsp;<a href=".$UrlBase."/seguridad/empresa/><img src='".$UrlBase.$SegPermiso->getImg_opcion()."' border='0' class='linkGrisBold11'/></a></td>";
                $html.="<td height='20' align='left' valign='middle'><a href='".$UrlBase.$SegPermiso->getRuta_opcion()."' class='linkGrisBold11'>&nbsp;&nbsp;".$SegPermiso->getNom_opcion()."</a></td>";
                $html.="</tr>";											
            }//end if
        }//end foreach
        $html.="</table>";
        return $html; 
    }//end function FibsysMenuIzquierdo()
    
}//Zend_View_Helper_FibsysMenuIzquierdo 
    //-------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------//

	
?>