<?php

/**
 * Permite generar el Menu Izquierdo de la pantalla principal
 * la cual permitira mostrar las opciones del modulo actual
 * y solo se mostrará al usuario siempre y cuando tenga permiso,
 * se debe de especificar el modulo para mostrar sus opciones.
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage MenuIzquierdoOpciones
 */

class Viamatica_MenuIzquierdoOpciones{
    /** @var Array */    
    private static $opciones = null;
        
                               
    private static function getOpciones() {
        self::$opciones = Viamatica_Data_Modulos::getData();
        
        return self::$opciones;
    }
    
    public static function generaHtml($cod_modulo){
        $defaultNS = new Zend_Session_Namespace('default');
        if ($defaultNS->cod_modulo==''){
            return '';
        }
        
        $translator = Zend_Registry::get('Zend_Translate');
        
        $helper_view = new Zend_View_Helper_BaseUrl();
        $BaseUrl  = $helper_view->baseUrl();

        $modulos = self::getOpciones();

        $html="<table width='155' border='0' cellspacing='0' cellpadding='0'>";

        $html.="<tr class='gris12bold' style='background:url(".$BaseUrl."/images/general/bg_tittle_menu.jpg); "
              ."background-repeat:repeat-x; padding-bottom:3px;'>" 
              ."<td style='padding-left:5px;'>::</td>"
              ."<td>".$translator->_($modulos[$cod_modulo]->titulo)."</td>"
              ."</tr>";

        foreach ($modulos[$cod_modulo]->opcion as $key => $reg) 
        {
            $html.="<tr>";
           /// $html.="<td align='left' valign='middle'>&nbsp;<a href=".$BaseUrl."/seguridad/empresa/><img src='".$BaseUrl.$SegPermiso->getImg_opcion()."' border='0' class='linkGrisBold11'/></a></td>";
            $link = $BaseUrl.$modulos[$cod_modulo]->ruta.$reg->ruta;
            $menubar = $BaseUrl.$modulos[$cod_modulo]->ruta.$reg->menubar;
            $html.="<td valign='middle'><a href='#' class='MenuIzquierdo' enlace='".$link."' menubar='".$menubar."'><img src='".$BaseUrl.Viamatica_Icono::RUTA_ICO_MODULOS.$reg->ico."' border='0' class='linkGrisBold11'/></a></td>";
            $html.="<td height='20' valign='middle' align='left'><a href=# class='MenuIzquierdo linkGrisBold11' enlace='".$link."' menubar='".$menubar."'>&nbsp;".$translator->_($reg->titulo)."</a></td>";
            $html.="</tr>";											
        }//end foreach
        $html.="</table>";
        
        unset($helper_view);
        unset($BaseUrl);
        return $html;         
    }
    
}

?>
