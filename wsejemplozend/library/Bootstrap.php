<?php


final class Bootstrap
{
    private static $_rootPath = null;


    public static function run(){
        self::inicializarPath();        
        require_once "Zend/Loader/Autoloader.php";       

        $loader = Zend_Loader_Autoloader::getInstance();
        $loader->setFallbackAutoloader(true);
        $loader->suppressNotFoundWarnings(false);

        self::inicializaConfiguracion();
        //self::inicializaEntornoPHP();            
        //self::inicializaLogger();     
        
        //self::inicializaConexion(5, 6, 7);

        self::inicializaFrontController();
    }//end function run()
    
    /**
     * Configura la ruta de los diferentes directorios que va a trabajar
     * la aplicacion
     */
    public static function inicializarPath(){
        self::$_rootPath = dirname(dirname(__FILE__));

        $paths = new ArrayObject();
        $paths->append(get_include_path());

        $paths->append(self::$_rootPath . '/application');
        $paths->append(self::$_rootPath . '/application/modules');
        $paths->append(self::$_rootPath . '/application/config');
        $paths->append(self::$_rootPath . '/library');
        


        set_include_path(implode(PATH_SEPARATOR, $paths->getArrayCopy()));
    }//end function incializarPath
    
    
    /**
     * Carga el archivo de configuración INI, el archivo que contiene todos
     * los datos de configuración de nuestro sistema, como por ejemplo layout,
     * información del timezone, textos, etc.
     * @return Zend_Config_Ini archivo de configuraciónm de la aplicación
     */
    public static function inicializaConfiguracion()
    {
        
        if (Zend_Registry::isRegistered('config')){
            return true;
        }//end if    

        $config = new Zend_Config_Ini("config.ini", "default");
        Zend_Registry::set("config", $config);
        return $config;
    }
    

//    /**
//     * Se configura el entorno de las variables de ambiente de PHP en ZendFramework
//     * como por ejemeplo si se presenta los errores en la aplicacion o el tiempo
//     * maximo de ejecucion de la apliacion
//     */
//    public static function inicializaEntornoPHP(){
//        //Indica si se presenta o no los errores
//        error_reporting(E_ALL | E_STRICT);        
//        ini_set('display_errors', Zend_Registry::get("config")->phpSettings->display_startup_errors);        
//        
//        //Establece el maximo tiempo de ejecucion
//        ini_set('max_execution_time', Zend_Registry::get("config")->phpSettings->max_execution_time);                
//    }//end function inicializaEntornoPHP

        
    
    /**
     * @param Zend_Registry $config
     * @param String $numero1
     * @param int $numero2
     */
    static function dividir(Zend_Config_Ini $config, $numero1, $numero2){  
        try{
            throw new Exception('division para CERO');
         }catch (Exception $e){
             echo('paso01');
             echo($e->getMessage());            
             throw $e;
         }//end try
    }
    

    
   public static function inicializaConexion($parametro1, $parametro2, $parametro3)
    {
       
        if (Zend_Registry::isRegistered('db')){
            return true;
        }//end if

        $config = Zend_Registry::get("config");

        try {
           // self::dividir($config, 5, 8);            
            $db = Zend_Db::factory($config->database);

            $conexion = $db->getConnection();

            Zend_Db_Table::setDefaultAdapter($db);

            Zend_Registry::set('db', $db);
    /*    } catch (Zend_Db_Adapter_Exception $ex) {		
            $obj_ex = new Viamatica_Exception($ex, Viamatica_Log::EMERG);
            var_dump($obj_ex->obtenerLogArray());
            var_dump($obj_ex->obtenerLogMensajeCompleto());
            var_dump($obj_ex->obtenerLogMensajeSimple());

            $logger->emerg('Emergency message');            
            throw new Zend_Db_Adapter_Exception("Mensaje:  " . $ex->getMessage());			
        } catch (Zend_Exception $ex) {
            $obj_ex = new Viamatica_Exception($ex, Viamatica_Log::EMERG);
            var_dump($obj_ex->obtenerLogArray());
            var_dump($obj_ex->obtenerLogMensajeCompleto());
            var_dump($obj_ex->obtenerLogMensajeSimple());

            $logger->emerg('Emergency message');            
            throw new Zend_Exception("Mensaje:  " . $ex->getMessage());
     */   } catch (Zend_Exception $ex) {
            $obj_ex = new Viamatica_Exception($ex, Viamatica_Log::EMERG);
            var_dump($obj_ex->obtenerLogArray());
            var_dump($obj_ex->obtenerLogMensajeCompleto());
            var_dump($obj_ex->obtenerLogMensajeSimple());
            
            return false;
        }
        return $db;
    }
    
    
    /**
     * Encargado de iniciar el Front Controller y despachar las peticiones (Dispatch).
     */
    public static function inicializaFrontController()
    {

        $frontController = Zend_Controller_Front::getInstance();   

        $frontController->addModuleDirectory(self::$_rootPath . "/application/modules");
       
        //Establecemos que se muestren las exceptiones (IMPORTANTE: PONER A FALSE EN PRODUCCION)
        $frontController->throwExceptions(true);
        
        $frontController->dispatch();
    }//end function inicializaFrontController

//    /**
//     * Se establece el logger a utilizar para la aplicacion, es util para depurar
//     * errores, advertencias, alertas, informativo.
//     */    
//    public static function inicializaLogger(){
//        $logger = new Zend_Log();
//
//        //Establece formato del logger
//        $format = '%timestamp% %priorityName% (%priority%): %message%' . PHP_EOL;
//        $formatter = new Zend_Log_Formatter_Simple($format);
//        
//        //Establece el escritor
//        $fecha = new DateTime();
//        $archivo_log = self::$_rootPath .'/logs/log-'.$fecha->format('Y-m-d').'.txt';
//        $writer = new Zend_Log_Writer_Stream($archivo_log);
//        $writer->setFormatter($formatter);        
//        
//        $logger->addWriter($writer);
//        
//        //Establece formato de la hora
//        $logger->setTimestampFormat("Y-m-d H:i:s");
//        
//        /*Tipos de mensajes para el log FILTRO
//         * 
//         *   Zend_Log::EMERG    0    (OK)
//         *   Zend_Log::ALERT    1
//         *   Zend_Log::CRIT     2    
//         *   Zend_Log::ERR      3    (OK)
//         *   Zend_Log::WARN     4    
//         *   Zend_Log::NOTICE   5
//         *   Zend_Log::INFO     6    
//         *   Zend_Log::DEBUG    7    (OK)
//         */
//
//        //Se establece filtros a nivel del escritor
//        $nivelLog = (int)Zend_Registry::get("config")->logger->nivel_mensaje;
//        $filter = new Zend_Log_Filter_Priority($nivelLog);
//        $writer->addFilter($filter);  
//        
//        Zend_Registry::set("logger", $logger);
//    
//        //Pruebas del Logger
////        $logger->info('Informational message');
////        $logger->emerg('Emergency message');        
//    }//end function inicializaLogger()
//    
}