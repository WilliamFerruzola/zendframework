<?php

/**
 * Permite acceder a los mensajes del sistema
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage Mensaje
 */


class Viamatica_Mensaje 
{    
    
    const ESTILO_CLASS_MENSAJE_OK       = 'exito';
    const ESTILO_CLASS_MENSAJE_INFO     = 'info';
    const ESTILO_CLASS_MENSAJE_ALERTA   = 'alerta';
    const ESTILO_CLASS_MENSAJE_ERROR    = 'error';

    static private $mensaje_ok_general   = 'Datos Grabados con Exito';
    static private $mensaje_ok_email     = 'Email enviado con Exito';    
    
    static public function getMensaje_ok_general() {
        $translator = Zend_Registry::get('Zend_Translate');          
        return $translator->_(self::$mensaje_ok_general);
    }

    static public function getMensaje_ok_email() {
        $translator = Zend_Registry::get('Zend_Translate');          
        return $translator->_(self::$mensaje_ok_email);        
    }


    
}//end class
