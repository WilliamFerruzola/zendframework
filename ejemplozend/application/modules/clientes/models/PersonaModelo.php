<?php

require_once "PersonaClass.php";

class PersonaModelo extends Zend_Db_Table
{
    protected $_name    = 'tbl_persona';
    protected $_primary = 'cod_persona'; //array('bug_id', 'product_id');

/*---------------------------------------------------------------------------------------*/
    public function obtenerTodos(){
	/*---------------------------------------------------------------------------------------*/
        return $this->fetchAll();
    }//end function obtenerTodos()
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
    public function obtenerPorId($id = 0){
	/*---------------------------------------------------------------------------------------*/
        return $this->find($id)->current();
    }//end function obtenerPorId
/*---------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------*/
    public function agregar(Persona $data ){
/*---------------------------------------------------------------------------------------*/	
        $registro = $this->createRow();
        $registro->nom_persona		= $data->getNom_persona();
        $registro->ape_persona		= $data->getApe_persona();
        $registro->edad                = $data->getEdad();
        $registro->sexo                = $data->getSexo();
        $registro->cod_pais		= $data->getCod_pais();
        $registro->cod_provincia	= $data->getCod_provincia();
        $registro->est_ecuatoriano	= $data->getEst_ecuatoriano();
        $registro->est_doble_nacionalidad= $data->getEst_doble_nacionalidad();

        $row = $registro->save();
        return $row;
    }//end function agregar
/*---------------------------------------------------------------------------------------*/
    
	
/*---------------------------------------------------------------------------------------*/	
    public function editar(Persona $data){
/*---------------------------------------------------------------------------------------*/	
        $registro                       = $this->find($data->getCod_persona())->current();

        $registro->nom_persona		= $data->getNom_persona();
        $registro->ape_persona		= $data->getApe_persona();
        $registro->edad                = $data->getEdad();
        $registro->sexo                = $data->getSexo();
        $registro->cod_pais		= $data->getCod_pais();
        $registro->cod_provincia	= $data->getCod_provincia();
        $registro->est_ecuatoriano	= $data->getEst_ecuatoriano();
        $registro->est_doble_nacionalidad= $data->getEst_doble_nacionalidad();
        
        $registro->save();
    }//end function editar
/*---------------------------------------------------------------------------------------*/	

/*---------------------------------------------------------------------------------------*/	
    public function eliminar($id){
/*---------------------------------------------------------------------------------------*/	
        $registro = $this->find($id)->current();
        if ($registro !== null) {
            $registro->delete();
        }//end if
    }//end function eliminar
/*---------------------------------------------------------------------------------------*/	

    
    public function listado(Persona $data){
        $select = $this->select();
        if ($data->getNom_persona()!=''){
            $select->where("nom_persona like ?","%".$data->getNom_persona()."%");
        }//end if
        if ($data->getApe_persona()!=''){
            $select->where("ape_persona like ?","%".$data->getApe_persona()."%");
        }//end if
        
        $sql = $select->__toString();
        return $this->fetchAll($select);
        
    }//end function listado

    
    public function listadoConJoin(Persona $data){
        
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from($this->_name, array('cod_persona','nom_persona','ape_persona','edad','sexo','est_ecuatoriano','est_doble_nacionalidad'));
        $select->joinLeft('tbl_provincia', 'tbl_provincia.cod_pais=tbl_persona.cod_pais '.
                                           ' and tbl_provincia.cod_provincia=tbl_persona.cod_provincia', array('nom_provincia'));
        if ($data->getNom_persona()!=''){
            $select->where("nom_persona like ?","%".$data->getNom_persona()."%");
        }//end if
        if ($data->getApe_persona()!=''){
            $select->where("ape_persona like ?","%".$data->getApe_persona()."%");
        }//end if
//        $select->order('nom_persona');
        $sql = $select->__toString();
        return $this->fetchAll($select);
        
    }//end function listadoConJoin

    
        public function listadoConPagineo(Persona $data, $nro_pagina){
        
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from($this->_name, array('cod_persona','nom_persona','ape_persona','edad','sexo','est_ecuatoriano','est_doble_nacionalidad'));
        $select->joinLeft('tbl_provincia', 'tbl_provincia.cod_pais=tbl_persona.cod_pais '.
                                           ' and tbl_provincia.cod_provincia=tbl_persona.cod_provincia', array('nom_provincia'));
        if ($data->getNom_persona()!=''){
            $select->where("nom_persona like ?","%".$data->getNom_persona()."%");
        }//end if
        if ($data->getApe_persona()!=''){
            $select->where("ape_persona like ?","%".$data->getApe_persona()."%");
        }//end if
//        $select->order('nom_persona');
        $sql = $select->__toString();
        $result =  $this->fetchAll($select);
        
        $paginator = Zend_Paginator::factory($result);
        
        $parametros = Zend_Registry::get('config');      
        
        $paginator->setItemCountPerPage($parametros->parametro->nro_registros);
        $paginator->setCurrentPageNumber($nro_pagina);

        return $paginator;
    }//end function listadoConJoin

}//end class EmpresaModelo
/*---------------------------------------------------------------------------------------*/	
