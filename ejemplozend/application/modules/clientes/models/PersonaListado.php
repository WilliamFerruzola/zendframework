<?php

class PersonaListado
{
    /** 
     * $tot_registros int
     */
    private $tot_registros  = null;

    /** 
     * $tot_pagina int
     */
    private $tot_pagina     = null;

    /** 
     * $Persona[] Persona
     */
    private $Persona     = null;
    
    
    public function getTot_registros() {
        return $this->tot_registros;
    }

    public function setTot_registros($tot_registros) {
        $this->tot_registros = $tot_registros;
    }

    public function getTot_pagina() {
        return $this->tot_pagina;
    }

    public function setTot_pagina($tot_pagina) {
        $this->tot_pagina = $tot_pagina;
    }

    /**
     *
     * @return Persona[]
     */
    public function getSegPersonas() {
        return $this->Persona;
    }

    /**
     *
     * @param Persona $Persona 
     */
    public function addSegPersona(Persona $Persona) {
        $this->Persona[] = $Persona;
    }

   
}//end class
/*---------------------------------------------------------------------------------------*/	

?>