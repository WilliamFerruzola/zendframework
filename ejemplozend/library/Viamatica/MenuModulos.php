<?php

/**
 * Permite generar el Menu de Modulos de la pantalla principal
 * la cual permitira mostrar todos los modulos que tenga el 
 * usuario permiso.
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage MenuModulos
 */
class Viamatica_MenuModulos{
    /** @var Array */    
    private static $modulos  = null;
                               
    private static function getModulos() {
        self::$modulos = Viamatica_Data_Modulos::getData();
        
        return self::$modulos;
    }
    
    
    public static function generaHtml(){
        //Ubico la ruta base utilizando el Helper de la Vista
        $helper_view = new Zend_View_Helper_BaseUrl();
        $urlBase = (string)$helper_view->baseUrl();
        unset($helper_view); //libera

        $modulos = self::getModulos();        
        
        $html = '<div id="site-bottom-bar" class="fixed-position" align="left">'
               .'   <div id="site-bottom-bar-frame"> '
               .'       <div id="site-bottom-bar-content" style="position:relative; bottom:14px; z-index:100;"> '
               .'           <a id="menu-root" href="#"> '
               .'               <img src="'.$urlBase .'/images/general/logo_modulos.png" border="0" /><br /> '
               .'               Módulos'
               .'            </a>'
               .'       </div> '
               .'       <div id="menu"> '
               .'           <table width="200" height="20" cellpadding="0" cellspacing="0"> '
               .'               <tr style="background:url('.$urlBase.'/images/general/icon_barra.jpg)"> '
               .'                   <td style="padding-left:9px"> '
               .'                         <span class="blanco12bold">Ir a...</span> '
               .'                   </td>'
               .'               </tr>'
               .'           </table>';

        foreach ($modulos as $key => $reg){
            $reg->titulo;
            $html .= '<a href="'.$urlBase.'/default/index/cambiarmodulo/cod_modulo/'.$reg->id.'" class="linkNegroBold12"><img src="'.$urlBase.'grafico.jpg" border="0" />'
                   . $reg->titulo
                   . '</a>'; 
        }//end foreach
            
        $html .= '     </div>'
               . '   </div>'
               . '</div>';
        unset($array);
        unset($urlBase);
        return $html;
    }//end function generaHtml

    
    /*public static function generaHtml($cod_modulo){
        $translator = Zend_Registry::get('Zend_Translate');
        
        $helper_view = new Zend_View_Helper_BaseUrl();
        $BaseUrl  = $helper_view->baseUrl();

        $modulos = self::getOpciones();
        

        $html="<table width='155' border='0' cellspacing='0' cellpadding='0'>";

        $html.="<tr class='gris12bold' style='background:url(".$BaseUrl."/images/general/bg_tittle_menu.jpg); "
              ."background-repeat:repeat-x; padding-bottom:3px;'>" 
              ."<td style='padding-left:5px;'>::</td>"
              ."<td>".$translator->_($modulos[$cod_modulo]->titulo)."</td>"
              ."</tr>";

        foreach ($modulos[$cod_modulo]->opcion as $key => $reg) 
        {
            $html.="<tr>";
           /// $html.="<td align='left' valign='middle'>&nbsp;<a href=".$BaseUrl."/seguridad/empresa/><img src='".$BaseUrl.$SegPermiso->getImg_opcion()."' border='0' class='linkGrisBold11'/></a></td>";
            $link = $BaseUrl.$modulos[$cod_modulo]->ruta.$reg->ruta;
            $menubar = $BaseUrl.$modulos[$cod_modulo]->ruta.$reg->menubar;
            $html.="<td valign='middle'><a href='#' class='MenuIzquierdo' enlace='".$link."' menubar='".$menubar."'><img src='".$BaseUrl.Viamatica_Icono::RUTA_ICO_MODULOS.$reg->ico."' border='0' class='linkGrisBold11'/></a></td>";
            $html.="<td height='20' valign='middle' align='left'><a href=# class='MenuIzquierdo linkGrisBold11' enlace='".$link."' menubar='".$menubar."'>&nbsp;".$translator->_($reg->titulo)."</a></td>";
            $html.="</tr>";											
        }//end foreach
        $html.="</table>";
        
        unset($helper_view);
        unset($BaseUrl);
        return $html;         
    }
     */
    
    
    
}

?>
