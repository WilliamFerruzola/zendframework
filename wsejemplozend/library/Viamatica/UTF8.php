<?php

/**
 * Permite generar la Barra de Menu Superior
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage BaseWWW

 */
class Viamatica_UTF8 {
    
     /**
      * Permite obtener de forma dinamica la ruta dinamica la ruta inicial del aplicativo
      * 
      * @return string 
      */
     public static function convertir(&$data){
        if ((is_object($data))||(is_array($data))){
            foreach ($data as &$value ){
                if ((is_object($value))||(is_array($value))){
                    Viamatica_UTF8::convertir($value);
                }else{
                    $value = mb_convert_encoding($value, "UTF-8");                
                }//end if
            }//end foreach
        }else{
            $data = mb_convert_encoding($data, "UTF-8");
        }//end if
    }//function  
    
}//end class

?>
