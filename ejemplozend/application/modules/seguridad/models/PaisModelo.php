<?php

require_once "PaisClass.php";

class PaisModelo extends Zend_Db_Table
{
    protected $_name    = 'tbl_pais';
    protected $_primary = 'cod_pais'; //array('bug_id', 'product_id');
    
/*---------------------------------------------------------------------------------------*/
    public function obtenerTodos(){
	/*---------------------------------------------------------------------------------------*/
        return $this->fetchAll();
    }//end function obtenerTodos()
/*---------------------------------------------------------------------------------------*/

    public function obtenerCombo($cod_pais = 0){
        $resulset = $this->obtenerTodos();

        $cod_pais_1eravez = 0;
        $option = "";
        foreach($resulset as $row){
            if ($cod_pais_1eravez == 0){
                $cod_pais_1eravez = $row['cod_pais'];
            }//end if

            $seleccionado = "";
            if ($row['cod_pais']==$cod_pais){
                $seleccionado = "selected";
            }//end if
            $option = $option."<option value='".$row["cod_pais"]."' ".$seleccionado.">".$row["nom_pais"]."</option>";
        }//end foreach

        return array('opciones'=>$option, 'primerCodigoPais'=>$cod_pais_1eravez) ;
    }//end function

    
    
/*---------------------------------------------------------------------------------------*/
    public function obtenerPorId($id = 0){
	/*---------------------------------------------------------------------------------------*/
        return $this->find($id)->current();
    }//end function obtenerPorId
/*---------------------------------------------------------------------------------------*/


}//end class PaisModelo
/*---------------------------------------------------------------------------------------*/	
