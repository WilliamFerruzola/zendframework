
<?php
require_once 'clientes/models/Persona.php';
require_once 'clientes/models/PersonaListado.php';
require_once 'clientes/models/structs/WsPersonaStruct.php';


/**
 * Permite acceder a los datos de la Cargo
 * por medio del uso de webServices al servidor de negocio
 * 
 * @author Arlett Carvajal 
 * @version 1.0
 * @package application_modules_seguridad_models_dao
 * @subpackage PersonaDAO
 */


class PersonaDAO extends Viamatica_BaseDAO
{

//    /**
//     * Obtiene el registro de la persona
//     * 
//     * @param integer $id  Codigo de la persona
//     * @return Persona
//     */
//    public function obtenerPorId($id)
//    {
//        
//            $uri = Viamatica_ServicioWeb::getUrlCapaNegocio('seguridad').'cargo?wsdl';
//            $client = new Zend_Soap_Client($uri,  array('encoding' => Viamatica_ServicioWeb::getCharset()));  //ISO-8859-1
//
//            $Viamatica_AutenticacionInfoUsuario = new Viamatica_AutenticacionInfoUsuario();
//            $usuario = $Viamatica_AutenticacionInfoUsuario->getUser();
//            unset($Viamatica_AutenticacionInfoUsuario); //libera
//            
//            $result_ws = $client->obtenerPorId( Viamatica_ServicioWeb::getUsuario('seguridad'),
//                                                Viamatica_ServicioWeb::getClave('seguridad'),
//                                                $usuario->cod_empresa,
//                                                $usuario->cod_usuario,
//                                                $id);
//            unset($usuario); //libera
//            
//            $this->_errorCodigo     = $result_ws->error_codigo;
//            $this->_errorMensaje    = $result_ws->error_mensaje;
//            
//            if ($result_ws->error_codigo == '00'){  //OK
//                $WsCargoStruct = new WsCargoStruct; 
//
//                $WsCargoStruct->cast($result_ws->registro);
//                    
//                //Se establece el objeto que va a contener el registro del webservice
//                $Persona = new Persona();
//                $Persona->setCod_cargo($WsCargoStruct->cod_cargo);
//                $Persona->setNom_cargo($WsCargoStruct->nom_cargo);
//                $Persona->setEst_cargo($WsCargoStruct->est_cargo);
//
//                
//                unset($WsCargoStruct);
//                unset($result_ws);
//                return  $Persona;
//            }else{
//                $this->_exception       = $result_ws->exception; 
//                
//                //Libera
//                unset($result_ws);
//            }//end if
//        
//    }//end function obtenerPorId
//    /*---------------------------------------------------------------------------------------*/

   /**
     * Obtiene un listado de los registros de personas
     * 
     * @param string  $idx             nombre del campo para el ordenamiento
     * @param string  $ord             forma del ordenamiento ASC o DESC
     * @param int     $pagina          Pagina que desea consultar
     * @param int     $reg_x_pagina    Numero de registros que se obtiene de la pagina
     * @param int     $cod_persona       Busqueda - Codigo de persona
     * @param string  $nom_persona       Busqueda - Nombre de persona
     * @return PersonaListado 
     */
    public function listado($idx, $ord, $pagina, $reg_x_pagina,
                            $cod_persona, $nom_persona
                           ){
    	 
            $uri = Viamatica_ServicioWeb::getUrlCapaNegocio('clientes')."persona?wsdl";
            $client = new Zend_Soap_Client($uri,  array('encoding' => Viamatica_ServicioWeb::getCharset())); 
            unset($uri); //Libera

            $result_ws = $client->listado( $idx,
                                           $ord,
                                           $pagina,
                                           $reg_x_pagina,
                                           $cod_persona,
                                           $nom_persona
                                         );			
            unset($usuario); //libera
            
            $this->_errorCodigo     = $result_ws->error_codigo;
            $this->_errorMensaje    = $result_ws->error_mensaje;
            
            if ($result_ws->error_codigo == "00"){  //OK
                $WsPersonaStruct = new WsPersonaStruct;
                
                $PersonaListado = new PersonaListado();
                $PersonaListado->setTot_pagina($result_ws->tot_paginas);
                $PersonaListado->setTot_registros($result_ws->tot_registros);
                
                if (!is_null($result_ws->registros)){
                    foreach ($result_ws->registros as $reg){
                        $WsPersonaStruct->cast($reg);

                        //Se establece el objeto que va a contener el registro del webservice
                        $Persona = new Persona();
                        $Persona->setCod_persona($WsPersonaStruct->cod_persona);
                        $Persona->setNom_persona($WsPersonaStruct->nom_persona);
                        $Persona->setApe_persona($WsPersonaStruct->ape_persona);
                        $Persona->setEdad($WsPersonaStruct->edad);
                        $Persona->setSexo($WsPersonaStruct->sexo);
                        $Persona->setEst_ecuatoriano($WsPersonaStruct->est_ecuatoriano);
                        $Persona->setEst_doble_nacionalidad($WsPersonaStruct->est_doble_nacionalidad);
                        $Persona->setNom_pais($WsPersonaStruct->nom_pais);
                        $Persona->setNom_provincia($WsPersonaStruct->nom_provincia);

                        $PersonaListado->addSegPersona($Persona);

                        unset($Persona);
                    }//end foreach
                }//end if
                
                //Libera las variables
                unset($result_ws);
                unset($WsPersonaStruct);
                
                return $PersonaListado;
            }else{
                $this->_exception       = $result_ws->exception;
                //Libera
                unset($result_ws);
            }//end if
            
        
    }//end function listado    



//    /**
//     * Permite realizar el matenimiento a la Cargo solo acepta 2 accion
//     * 
//     *    I =>  Ingresar
//     * 
//     *    M =>  Modificar
//     * 
//     *    E =>  Eliminacion Logica
//     * 
//     * @param String $accion   I => Ingresar   M => Modificar
//     * @param Persona $Persona Registro 
//     * @return Persona
//     */
//    public function mantenimiento($accion, Persona $Persona){
//    /*---------------------------------------------------------------------------------------*/	
//        
//            $uri = Viamatica_ServicioWeb::getUrlCapaNegocio('seguridad')."cargo?wsdl";
//            $client = new Zend_Soap_Client($uri,  array('encoding' => Viamatica_ServicioWeb::getCharset()));  //ISO-8859-1
//
//            $Viamatica_AutenticacionInfoUsuario = new Viamatica_AutenticacionInfoUsuario();
//            $usuario = $Viamatica_AutenticacionInfoUsuario->getUser();
//            unset($Viamatica_AutenticacionInfoUsuario); //libera
//
//            $result_ws = $client->mantenimiento(Viamatica_ServicioWeb::getUsuario('seguridad'), 
//                                                                    Viamatica_ServicioWeb::getClave('seguridad'),
//                                                                    $usuario->cod_empresa,
//                                                                    $usuario->cod_usuario,
//                                                                    $accion,
//                                                                    $Persona->getCod_cargo(),
//                                                                    $Persona->getNom_cargo(),
//                                                                    $Persona->getEst_cargo()
//                                                                    );	
//            $this->_errorCodigo     = $result_ws->error_codigo;
//            $this->_errorMensaje    = $result_ws->error_mensaje;
//            
//            if ($result_ws->error_codigo == "00"){  //OK
//
//                $WsCargoStruct = new WsCargoStruct;
//                $WsCargoStruct->cast($result_ws->registro);
//
//                //Se establece el objeto que va a contener el registro del webservice
//                $Persona = new Persona();
//                $Persona->setCod_cargo($WsCargoStruct->cod_cargo);
//                $Persona->setNom_cargo($WsCargoStruct->nom_cargo);
//                $Persona->setEst_cargo($WsCargoStruct->est_cargo);
//                
//                unset($result_ws); //libera
//                return $Persona;
//            }else{
//                $this->_exception       = $result_ws->exception;
//                unset($result_ws); //libera   
//            }//end if
//            /*--------------------------------------------------------------------------------------*/
//        
//    /*---------------------------------------------------------------------------------------*/	
//    }//end function matenimiento
//    /*---------------------------------------------------------------------------------------*/	
//
////    
////    
////    /**
////     * Obtiene el número de registros que procesará el grid de Perfils
////     * 
////     * @param String $where Se envia condiciones adicionales para el filtro de registros
////     * @return integer
////     */
////    public function getGridRowCount($where){
////        try{  	 
////            $uri = Viamatica_ServicioWeb::getUrlCapaNegocio('seguridad')."Perfil?wsdl";
////            $client = new Zend_Soap_Client($uri,  array('encoding' => Viamatica_ServicioWeb::getCharset()));  //ISO-8859-1
////            unset($uri); //libera
////            
////            $Viamatica_AutenticacionInfoUsuario = new Viamatica_AutenticacionInfoUsuario();
////            $usuario = $Viamatica_AutenticacionInfoUsuario->getUser();
////            unset($Viamatica_AutenticacionInfoUsuario);
////                        
////            $result_ws = $client->getGridRowCount(  Viamatica_ServicioWeb::getUsuario('seguridad'), 
////                                                    Viamatica_ServicioWeb::getClave('seguridad'),
////                                                    $usuario->cod_Perfil,
////                                                    $usuario->cod_usuario,
////                                                    $where
////                                                  );	
////            unset($usuario); //libera
////            
////            if ($result_ws->error_codigo == "00"){  //OK         
////                $nro_registro = $result_ws->nro_registro;
////                unset ($result_ws);//libera
////                
////                return  $nro_registro;
////            }else{
////                $this->_errorCodigo     = $result_ws->error_codigo;
////                $this->_errorMensaje    = $result_ws->error_mensaje;
////                unset($result_ws); //libera
////                
////                throw new Exception("Error Codigo:".$result_ws->error_codigo." - ".$result_ws->error_mensaje);	
////            }//end if
////            /*--------------------------------------------------------------------------------------*/
////        }catch (Zend_Exception $e) {
////            $this->_errorCodigo     = $result_ws->error_codigo;
////            $this->_errorMensaje    = $result_ws->error_mensaje;
////            echo "Error message (pruebaAction): " , $e->getTraceAsString() , "\n";  
////        }  	
////    /*---------------------------------------------------------------------------------------*/	
////    }//end function getGridRowCount
////    /*---------------------------------------------------------------------------------------*/	
//
//    
//     /**
//     * Obtiene todos los cargo
//     * 
//     * @return Persona[]
//     */
//    public function obtenerTodos()
//    {
//
//            $uri = Viamatica_ServicioWeb::getUrlCapaNegocio('seguridad').'cargo?wsdl';
//            
//            $client = new Zend_Soap_Client($uri,  array('encoding' => Viamatica_ServicioWeb::getCharset()));  //ISO-8859-1
//
//            $Viamatica_AutenticacionInfoUsuario = new Viamatica_AutenticacionInfoUsuario();
//            $usuario = $Viamatica_AutenticacionInfoUsuario->getUser();
//            unset($Viamatica_AutenticacionInfoUsuario); //libera
//            
//           $result_ws = $client->obtenerTodos( Viamatica_ServicioWeb::getUsuario('seguridad'),
//                                                Viamatica_ServicioWeb::getClave('seguridad'),
//                                                $usuario->cod_empresa,
//                                                $usuario->cod_usuario
//                                              );
//            unset($usuario); //libera
//            
//           $this->_errorCodigo     = $result_ws->error_codigo;
//           $this->_errorMensaje    = $result_ws->error_mensaje;
//            
//            if ($result_ws->error_codigo == '00'){  //OK
//                $WsCargoStruct = new WsCargoStruct();
//                
//                foreach ($result_ws->registros as $key => $reg){                    
//                    $WsCargoStruct->cast($reg);    
//                    
//                    $Persona = new Persona();
//                    $Persona->setCod_cargo($WsCargoStruct->cod_cargo);
//                    $Persona->setNom_cargo($WsCargoStruct->nom_cargo);
//                    $Persona->setEst_cargo($WsCargoStruct->est_cargo);
//                    
//                    $arrPersona[] = $Persona;
//                    unset($Persona);
//                }//end foreach
//
//                unset($result_ws); //libera
//                return  $arrPersona;
//            }else{
//                $this->_exception       = $result_ws->exception;
//            //libera
//            unset($result_ws);
//        }
//    }//end function obtenerTodos
    
    
    
}//end class
