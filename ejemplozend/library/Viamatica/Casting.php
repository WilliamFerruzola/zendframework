<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Casting
 *
 * @author curso
 */
abstract class Viamatica_Casting {
    
    public function cast ($obj){
        // check that each of the passed parameters are valid before setting the
        // appropriate class variable.
        foreach ( $obj as $var => $value ){
            $this->$var = $value;
        }
    }//end function cast
    
}//end class Casting

?>
