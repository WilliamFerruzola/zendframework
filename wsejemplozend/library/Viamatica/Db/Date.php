<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Obtiene la fecha actual del sistema NOW() Mysql,   getDate() SqlServer, etc.
 *
 * @package Viamatica_Db
 * @subpackage Date
 * @author David Salazar
 */

/**
 * @return Zend_Db_Expr
 */
class Viamatica_Db_Date extends Zend_Db_Table
{
    public static function getDate($adaptador){
        $tipo_adapter =  get_class($adaptador);
        
        switch ($tipo_adapter){
            case 'Zend_Db_Adapter_Pdo_Mysql':
                $expr =  new Zend_Db_Expr('NOW()');
                break;
            
            case 'Zend_Db_Adapter_Pdo_Sqlsrv':
                $expr =  new Zend_Db_Expr('getDate()'); //revisar
                break;
                
            case 'Zend_Db_Adapter_Pdo_Oracle':
                break;
                
        }//end switch
        
        unset($tipo_adapter); //libera
        return $expr;
    }//end function getDate
}//end class

?>
