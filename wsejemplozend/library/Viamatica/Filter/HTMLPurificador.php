<?php

class Viamatica_Filter_HTMLPurificador implements Zend_Filter_Interface {

    public function filter($value){
        $search  = array('script', '"', "'", '<body>');
        $replace = array('', '', '', '', '');
        $value = str_replace($search, $replace, $value);        
        
        return $value;
    }//end public function filter
}

?>
