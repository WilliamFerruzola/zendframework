<?php

class PersonaDAO extends Zend_Db_Table
{
    protected $_name    = 'tbl_persona';
    protected $_primary = 'cod_persona'; //array('bug_id', 'product_id');

  
        /**
         * Obtiene el listado de persona
         * 
         * @param string  $campo_orden            Campo Clave para realizar la Busqueda
         * @param string  $modo_orden             Campo Ordenamiento
         * @param int     $nro_pagina             Pagina donde debe de iniciar el pagineo
         * @param int     $nroreg_x_pagina        Establece el número de registros que debe traer cada pagina
         * @param int     $cod_persona            Busqueda - codigo de persona 
         * @param string  $nom_persona            Busqueda - Nombre de persona 
         * @return Zend_Paginator 
         */
        public function listado($campo_orden,
                                $modo_orden,
                                $nro_pagina,
                                $nroreg_x_pagina,
                                $cod_persona, 
                                $nom_persona
                               ){
            
     
        
        $select = $this->select();
        $select->setIntegrityCheck(false);
        

        $select->from('tbl_persona', 'tbl_persona.*');
        $select->joinLeft('tbl_pais', 'tbl_pais.cod_pais = tbl_persona.cod_pais', array('nom_pais'));
        $select->joinLeft('tbl_provincia', 'tbl_provincia.cod_provincia = tbl_persona.cod_provincia', array('nom_provincia'));
        $select->where("tbl_pais.cod_pais=tbl_provincia.cod_pais");
        
        if (!(empty($cod_persona))){
             $select->where("cod_persona like ?","%".$cod_persona."%");
        }//end if
        
        if (!(empty($nom_persona))){
             $select->where("nom_persona like ?","%".$nom_persona."%");
        }//end if
        
        $result =  $this->fetchAll($select);
        $paginator = Zend_Paginator::factory($result);
        
        $paginator->setItemCountPerPage($nroreg_x_pagina);
        $paginator->setCurrentPageNumber($nro_pagina);

        unset($result);  //liberar
        return $paginator;
        
    }//end function listado
    
   
}//end class PersonaDAO
/*---------------------------------------------------------------------------------------*/	
