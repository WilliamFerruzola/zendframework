<?php


class Viamatica_View_Helper_MenuIzquierdoOpciones{

    /**
     * MenuIzquierdoOpciones
     * @param integer $cod_modulo
     * @return String
     */    
    public function MenuIzquierdoOpciones($cod_modulo)
    {
        $html = Viamatica_MenuIzquierdoOpciones::generaHtml($cod_modulo);

        return $html;
    }//end function ViamaticaMenuBarSuperior()

}//Zend_View_Helper_ViamaticaMenuBarSuperior

?>