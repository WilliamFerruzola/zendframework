<?php

/**
 * Permite acceder a metodos comunes del esquema BO
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage BaseBO
 */


class Viamatica_BaseBO 
{
    /**
     * Tendrá la conexion de la base de datos
     * 
     * @var Viamatica_ConexionDB 
     */
    protected $ConexionDB      = null;
   
    
    /**
     * Si ocurre algun error, en este campo se almacenará el CODIGO DEL ERROR
     *
     * @var String
     */    
    protected $_errorCodigo    = null;  

    /**
     * Si ocurre algun error, en este campo se almacenará la DESCRIPCION DEL ERROR
     *
     * @var String
     */    
    protected $_errorMensaje   = null;    

    /**
     * Obtiene el codigo de Error
     *
     * @return String
     */        
    public function getErrorCodigo(){
        return $this->_errorCodigo;
    }

    /**
     * Obtiene el mensaje de Error
     *
     * @return String
     */            
    public function getErrorMensaje(){
        return $this->_errorMensaje;
    }

    /**
     *
     * @param stdClass $obj 
     */
    public function registrarException($e, &$obj){
        //registra la excepcion en el log
        $obj_ex = new Viamatica_Exception($e, Viamatica_Log::EMERG);

        $obj->error_codigo           = Viamatica_Respuesta::CODIGO_ERROR;
        $obj->error_mensaje          = $e->getMessage();
        $obj->exception              = $obj_ex->obtenerLogArray();        
    }//end function registrarException
    
}//end class