<?php

class LoginModelo{
    const IDENTIDAD_NO_ENCONTRADA = 'El usuario no existe en el sistema';
    const CLAVE_INCORRECTA = 'La clave introducida no es correcta';
    const USUARIO_INCORRECTO = 'El usuario es incorrecto';
    const LOGIN_INCORRECTO = 'El login no se ha podido realizar';

/*---------------------------------------------------------------------------------------*/
    /**
     * Realiza el login en la aplicación los los datos pasados.
     * @param String $usr_name Email de usuario
     * @param String $clave Contraseña del usuario
     * @return LoginModelo
     */
/*---------------------------------------------------------------------------------------*/	 
    public function login($usr_name, $clave){
/*---------------------------------------------------------------------------------------*/	
	//	echo("<br>paso01  usr_name:".$usr_name."*clave:".$clave);
        if (!empty($usr_name) && !empty($clave)) {
            $adapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
            $adapter->setTableName('tbl_usuario');
            $adapter->setIdentityColumn('usr_name');
            $adapter->setCredentialColumn('usr_pass');
            $adapter->setIdentity($usr_name);
            $adapter->setCredential($clave);

            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter);
			
            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    throw new Exception(self::IDENTIDAD_NO_ENCONTRADA);
                    break;
                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    throw new Exception(self::CLAVE_INCORRECTA);
                    break;
                case Zend_Auth_Result::SUCCESS:
                    if ($result->isValid()) {
                        $data = $adapter->getResultRowObject();
                        $auth->getStorage()->write($data);
                    } else {
                        throw new Exception(self::USUARIO_INCORRECTO);
                    }
                    break;
                default:
                    throw new Exception(self::LOGIN_INCORRECTO);
                    break;
            }
        } else {
            throw new Exception(self::LOGIN_INCORRECTO);
        }

        return $this;
    }
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
/*  Obtiene la identidad del usuario si existe.											 */
/*  @return <type> La identidad del usuario si existe									 */
/*---------------------------------------------------------------------------------------*/	 
    public static function getIdentidad(){
/*---------------------------------------------------------------------------------------*/	
        return Zend_Auth::getInstance()->getIdentity();
    }//end function getIdentidad
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
/* Indica si el usuario está o no logueado en la aplicación.							 */
/* @return boolean <b>true</b> Si está logueado									 		 */
/*                 <b>false</b> En otro caso											 */
/*---------------------------------------------------------------------------------------*/	 
    public static function estaLogueado(){
/*---------------------------------------------------------------------------------------*/	
        return Zend_Auth::getInstance()->hasIdentity();
    }//end function estaLogueado
/*---------------------------------------------------------------------------------------*/	


/*---------------------------------------------------------------------------------------*/
    public function logout(){
/*---------------------------------------------------------------------------------------*/	
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy(true);
        return $this;
    }//end function logout
/*---------------------------------------------------------------------------------------*/	



}//end class LoginModelo
