<?php


class Viamatica_View_Helper_MenuBarSuperior{

    /**
     * MenuBarSuperior
     * @param Viamatica_MenuBarSuperior $data
     * @return String
     */    
    public function MenuBarSuperior(Viamatica_MenuBarSuperior $data)
    {
        $html = $data->getMenuBar($data);
        
        return $html;
    }//end function ViamaticaMenuBarSuperior()

}//Zend_View_Helper_ViamaticaMenuBarSuperior

?>