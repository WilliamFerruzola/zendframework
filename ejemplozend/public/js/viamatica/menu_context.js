/*------------------------------------------------------------------------------------------------------------------*/
/* Permite activar el menu contextual al momento de realizar click izquierdo sobre el formulario */
/*------------------------------------------------------------------------------------------------------------------*/
function MenuContext_Inicializar(){ 
    // Esta funcion permite ejecutar el evento seleccionado
    $("#ContextMenu").contextMenu({menu: 'div_Menu_Contextual'},
                                    function(action, el, pos) {                                       
                                            $("."+action).click();
                                    }//end function
     );//End contextMenu
}//End ContextMenuEvent();		


/*------------------------------------------------------------------------------------------------------------------*/
/* Permite anexar eventos a los botones */
/*------------------------------------------------------------------------------------------------------------------*/
function MenuContext_Button_EventClickUp(boton, funcion){
    $("."+boton).unbind();
    $("."+boton).mouseup(funcion);
}//end MenuBar_Button_EventClick


/*------------------------------------------------------------------------------------------------------------------*/
/* Permite habilitar botones */
/*------------------------------------------------------------------------------------------------------------------*/
function MenuContext_EnableButton(control, habilitar){
    if (habilitar){
        $('#div_Menu_Contextual').enableContextMenuItems('#'+control);
    }else{
        $('#div_Menu_Contextual').disableContextMenuItems('#'+control);
    }
}


/*------------------------------------------------------------------------------------------------------------------*/
/* Permite habilitar botones */
/*------------------------------------------------------------------------------------------------------------------*/
function MenuContext_EnableButtons(habilitar){
    if (habilitar){
        $('#div_Menu_Contextual').enableContextMenuItems();
    }else{
        $('#div_Menu_Contextual').disableContextMenuItems();
    }
}//end function MenuContext_EnableButtons
