<?php

class Persona{
    private $cod_persona;
    private $nom_persona;
    private $ape_persona;
    private $edad;
    private $sexo;
    private $cod_pais;
    private $cod_provincia;
    private $est_ecuatoriano;
    private $est_doble_nacionalidad;
    
    private $nom_pais;
    private $nom_provincia;
    


    public function __construct(){
    /*---------------------------------------------------------------------------------------*/								
    }//end function __construct
    /*---------------------------------------------------------------------------------------*/

    public function getCod_persona() {
        return $this->cod_persona;
    }

    public function setCod_persona($cod_persona) {
        $this->cod_persona = $cod_persona;
    }

    public function getNom_persona() {
        return $this->nom_persona;
    }

    public function setNom_persona($nom_persona) {
        $this->nom_persona = $nom_persona;
    }

    public function getApe_persona() {
        return $this->ape_persona;
    }

    public function setApe_persona($ape_persona) {
        $this->ape_persona = $ape_persona;
    }

    public function getEdad() {
        return $this->edad;
    }

    public function setEdad($edad) {
        $this->edad = $edad;
    }

    public function getSexo() {
        return $this->sexo;
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    public function getCod_pais() {
        return $this->cod_pais;
    }

    public function setCod_pais($cod_pais) {
        $this->cod_pais = $cod_pais;
    }

    public function getCod_provincia() {
        return $this->cod_provincia;
    }

    public function setCod_provincia($cod_provincia) {
        $this->cod_provincia = $cod_provincia;
    }

    public function getEst_ecuatoriano() {
        return $this->est_ecuatoriano;
    }

    public function setEst_ecuatoriano($est_ecuatoriano) {
        $this->est_ecuatoriano = $est_ecuatoriano;
    }

    public function getEst_doble_nacionalidad() {
        return $this->est_doble_nacionalidad;
    }

    public function setEst_doble_nacionalidad($est_doble_nacionalidad) {
        $this->est_doble_nacionalidad = $est_doble_nacionalidad;
    }

    public function getNom_pais() {
        return $this->nom_pais;
    }

    public function setNom_pais($nom_pais) {
        $this->nom_pais = $nom_pais;
    }

    public function getNom_provincia() {
        return $this->nom_provincia;
    }

    public function setNom_provincia($nom_provincia) {
        $this->nom_provincia = $nom_provincia;
    }


}//end Class Persona


?>