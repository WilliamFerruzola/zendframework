<?php

/**
 * Permite generar el Menu Izquierdo de la pantalla principal
 * la cual permitira mostrar las opciones del modulo actual
 * y solo se mostrará al usuario siempre y cuando tenga permiso,
 * se debe de especificar el modulo para mostrar sus opciones.
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage MenuIzquierdoOpcionesPopup

 */
class Viamatica_MenuIzquierdoOpcionesPopup{
        
                               
    /**
     * Obtiene el Menú Izquierdo del Popup de las ventanas
     *
     * @param Array $array  Contiene informacion del Menú del Popup del registro para presentar información de los menús relacionados
     *       La estructura del Array se encuentra estructurado de la siguiente manera el cual es procesado por un  "foreach ($array as $reg)"
     *       donde $reg es el registro y tiene lo siguiente:
     *
     *          $reg[0] == 'TITULO'     (Posicion CERO si tiene el valor de TITULO indica que es un título,
     *                     'OPCION'      si tiene el valor de OPCION será una opción del menú principal=
     *          $reg[1]                 Contiene el valor que deberá de escribirse en el menú que verá el usuario
     *          $reg[2]                 Contiene la URL que tendrá relacionado con el texto que se indique en $reg[1]
     *
     *       El menú que se visualizará tendrá la siguiente estructura de acuerdo a los valores que pasen en el parámetro $array:
     *
     *          General                  ($reg[0]  => TITULO)
     *             Definición Empresa    ($reg[1]  => Contenido  + URL o Javascript relacionada en $reg[2] + indicar Javascript en $reg[3])
     *          Contabilidad    (TITULO) ($reg[0]  => TITULO)
     *             Máscara Plan Cuentas  ($reg[1]  => Contenido  + URL relacionada en $reg[2])
     *             Definición de Moneda  ($reg[2]  => Contenido  + URL relacionada en $reg[2])
     *
     *
     * @return String  Devolver el menú en codificacion HTML el mismo que deberá ser visualizado por la vista por medio de un ECHO
     */
    public static function generaHtml($array){
        $html ="<table width='100%' border='0' cellspacing='0' cellpadding='0' align='left'>";
        foreach ($array as $reg){
            switch($reg[0]){
                case 'TITULO':
                    $html.="<tr>";
                    $html.="<td heigth='40' class='celeste13bold' align='left'><p>&nbsp;&nbsp;".$reg[1]."</p></td>";
                    $html.="</tr>";
                    break;

                case 'OPCION':
                    $html.="<tr>";
                    $html.="<td align='left' valign='middle'>";
                    $html.="<a href='javascript:".$reg[2]."' class='linkGrisBold11'>";
                    $html.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$reg[1];
                    $html.="</a>";
                    $html.="</td>";
                    $html.="</tr>";
                    break;
            }//end switch

            unset($reg); //libera
        }//end foreach
        $html.="</table>";

        unset($array);
        return $html;
    }//end function
    
}//end class

?>
