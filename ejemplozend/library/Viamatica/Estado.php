<?php

/**
 * Permite acceder al combo de estado
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage FibsysEstado
 */


class Viamatica_Estado 
{    
   
    /**
     * Obtiene el combo de estados
     * 
     * @param integer $cod_pais  Codigo del Pais
     * @param string  $primerElemento  Primer Elemento del Combo (opcional)
     * @return Array
     */    
    public static function obtenerCombo($primerElemento = ''){
        $data = Array('A'=>'Activo',
                      'I'=>'Inactivo'
                      );
        
        $cbo = Array();

        if ($primerElemento != ''){
            $cbo[''] = $primerElemento;
        }//end if
        
        foreach($data as $key => $dato){
            $cbo[$key] = $dato;
        }//end foreach

        return $cbo;
    }//end function obtenerCombo
 
    
    
    /**
     * Obtiene el combo de estados de  tipo de usuario
     * 
     * @param integer $cod_pais  Codigo del Pais
     * @param string  $primerElemento  Primer Elemento del Combo (opcional)
     * @return Array
     */    
    public static function obtenerComboTipoUsuario($primerElemento = ''){
        $data = Array('E'=>'Estandar',
                      'A'=>'Administrador'
                      );
        
        $cbo = Array();

        if ($primerElemento != ''){
            $cbo[''] = $primerElemento;
        }//end if
        
        foreach($data as $key => $dato){
            $cbo[$key] = $dato;
        }//end foreach

        return $cbo;
    }//end function obtenerComboTipoUsuario
    


}//end class
