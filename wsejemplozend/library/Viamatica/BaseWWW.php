<?php

/**
 * Permite generar la Barra de Menu Superior
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage BaseWWW

 */
class Viamatica_BaseWWW {
    
     /**
      * Permite obtener de forma dinamica la ruta dinamica la ruta inicial del aplicativo
      * 
      * @return string 
      */
     public static function getBaseWWW(){
		$dato = "";
		if (empty($_SERVER['HTTPS'])||($_SERVER['HTTPS']=='off')){
			$dato = "http://";
		}else{
			$dato = "https://";		
		}//end if

        $dato =  $dato.$_SERVER['SERVER_NAME'];

		if ($_SERVER['SERVER_PORT']!=80){
			if (empty($_SERVER['HTTPS'])||($_SERVER['HTTPS']=='off')){		
				$dato = $dato.":".$_SERVER['SERVER_PORT'];
			}//end if
		}//end if

        return $dato;
    }//function BaseWWW()   
    
}//end class

?>
