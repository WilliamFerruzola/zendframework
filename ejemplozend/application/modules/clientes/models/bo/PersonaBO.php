<?php

require_once 'clientes/models/dao/PersonaDAO.php';


class PersonaBO extends Viamatica_BaseBO
{

//    /**
//     * Obtiene el registro de la persona
//     * 
//     * @param integer $id  Codigo de la persona
//     * @return Persona
//     */
//    public function obtenerPorId($id)
//    {
//        
//            $PersonaDAO = new PersonaDAO();
//            
//            $data = $PersonaDAO->obtenerPorId($id);
//            
//            $this->procesarException($PersonaDAO);  
//            
//            return $data;
//
//      
//    }//end function obtenerPorId
    
    
    /*---------------------------------------------------------------------------------------*/
    /**
     * Obtiene un listado de los registros de personas
     * 
     * @param string  $idx               nombre del campo para el ordenamiento
     * @param string  $ord               forma del ordenamiento ASC o DESC
     * @param int     $pagina            Pagina que desea consultar
     * @param int     $reg_x_pagina      Numero de registros que se obtiene de la pagina
     * @param int     $cod_persona       Busqueda - Codigo de cargo
     * @param string  $nom_persona       Busqueda - Nombre de cargo
     * @return PersonaListado 
     */
    public function listado($sidx, $sord, $pagina, $reg_x_pagina,
                            $cod_persona, $nom_persona
                            ){
    
            $PersonaDAO = new PersonaDAO();
                        
            $data = $PersonaDAO->listado($sidx, $sord, $pagina, $reg_x_pagina,
                                            $cod_persona, $nom_persona
                                            );
            $this->_errorCodigo     = $PersonaDAO->getErrorCodigo();
            $this->_errorMensaje    = $PersonaDAO->getErrorMensaje();

            $this->procesarException($PersonaDAO);
             
            return $data;
    }//end listado
    
    
    
 /*---------------------------------------------------------------------------------------*/	
//    /**
//     * Permite realizar el matenimiento a la Cargo solo acepta 2 accion
//     * 
//     *    I =>  Ingresar
//     * 
//     *    M =>  Modificar
//     * 
//     *    E =>  Eliminacion Logica
//     * 
//     * @param String $accion   I => Ingresar   M => Modificar   E=> Eliminar
//     * @param Persona  Registro de Cargo
//     * @return Persona
//     */
//    public function mantenimiento($accion, Persona $Persona){
//   
//      
//            $PersonaDAO = new PersonaDAO();
//            $data = $PersonaDAO->mantenimiento($accion, $Persona);
//
//            $this->_errorCodigo     = $PersonaDAO->getErrorCodigo();
//            $this->_errorMensaje    = $PersonaDAO->getErrorMensaje();
//            
//            $this->procesarException($PersonaDAO);
//            
//            unset($PersonaDAO);//libera
//            
//            return $data;
//       
//    }//end function mantenimiento
//    
//    
//    
//    
//    /*---------------------------------------------------------------------------------------*/	
//     /**
//     * Obtiene todos los cargo
//     * 
//     * @return Persona[]
//     */
//    public function obtenerTodos()
//    {
//        $PersonaDAO = new PersonaDAO();
//        
//        return $PersonaDAO->obtenerTodos(); 
//        
//        $this->procesarException($PersonaDAO);
//    }//end function obtenerTodos    
//    
//    /**
//     * Obtiene el combo de cargo
//     * 
//     * @param integer $cod_cargo  Codigo del cargo
//     * @param string  $primerElemento  Primer Elemento del Combo (opcional)
//     * @return Array
//     */    
//    public function obtenerCombo($primerElemento = ''){
//        $result = $this->obtenerTodos();
//        
//        $cbo = Array();
//
//        if ($primerElemento != ''){
//            $cbo[''] = $primerElemento;
//        }//end if
//
//        foreach($result as $key => $reg){
//            $cbo[$reg->getCod_cargo()] = $reg->getNom_cargo();
//        }//end foreach
//
//        unset($result); //libera
//        return $cbo;
//    }//end function obtenerCombo
    
}//end class    
?>
