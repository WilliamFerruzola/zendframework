/*
 *Este script necesita:
 *
 * - librerias 
 *          /public/js/viamatica/jquery_repeat.js
 *          
 * - Variables Global Javascript que contiene la instancia de los controles
 *          var control_nivel_eliminar
 *          var control_nivel_secuencia
 *          var control_nivel_longitud         
 */


/*-------------------------------------------------------------------------------------------------------------------------------------*/
//Funcion Ready Jquery
/*-------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    //Cambia el comportamiento de los botones
    //$("button").button();
    $("#agregarRow").button();
    $("#agregarRow").removeClass('ui-button-text-only');
    $("#agregarRow").css('heigth','27px');
    $("#agregarRow").css('width','23px');
    $("#agregarRow").click(agregarRow);

    $('.nivel_longitud').live('change',function(){
        mostrar_formato();
        HabilitarBotonGrabar();
    });

    setComportamientoRowEliminar();
    
    //Anexa al manejar change
    $("#frm :input").change(HabilitarBotonGrabar);

    //Realiza el trim a todos los controles input
    inicializarTrimControles(); 
    
    // binds form submission and fields to the validation engine
    jQuery("#frm").validationEngine();

    //Menu Superior
    MenuBar_Button_EventClickUp('principal','opciones','grabar',MenuBar_Grabar_OnClick);
    MenuBar_Button_EventClickUp('principal','opciones','salir',MenuBar_Salir_OnClick);

    //Menu Contextual
    MenuContext_Inicializar();	
    MenuContext_Button_EventClickUp('MenuContextual_Nuevo',MenuBar_Nuevo_OnClick);    
    MenuContext_Button_EventClickUp('MenuContextual_Grabar',MenuBar_Grabar_OnClick);
    MenuContext_Button_EventClickUp('MenuContextual_Eliminar',MenuBar_Eliminar_OnClick);
    MenuContext_Button_EventClickUp('MenuContextual_Salir',MenuBar_Salir_OnClick);

    MenuContext_EnableButton('MenuContextual_Nuevo',false);
    MenuContext_EnableButton('MenuContextual_Eliminar',false);
    MenuContext_EnableButton('MenuContextual_Salir', true);    


    $('#formato').attr('readonly','readonly');

    setComportamientoFormulario();
//    $('#nom_empresa').focus();


/*-------------------------------------------------------------------------------------------------------------------------------------*/
});//End Funcion Ready Jquery
/*-------------------------------------------------------------------------------------------------------------------------------------*/

function setComportamientoRowEliminar(){
    $(".remove_row_nivel").button();
    $(".remove_row_nivel").removeClass('ui-button-text-only');
    $(".remove_row_nivel").css('heigth','27px');
    $(".remove_row_nivel").css('width','25px');

    $('.remove_row_nivel').live('click',function(){
       $(this).parent().parent().remove();  //Elimina la fila       
        renumerar_secuencia();
        mostrar_formato();
        HabilitarBotonGrabar();
    });  

}//end function setComportamientoFilas


/*-------------------------------------------------------------------------------------------------------------------------------------*/
//Se realiza el submit del formulario a traves de ajax
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function submit_formulario(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    //Se llama al loader
    ViamaticaMensajeCargandoVisible(true);
    
    //Se desactivan las opciones del MenuBur y MenuContext
    MenuBar_EnableButtons(false);
    MenuContext_EnableButtons(false); 

    $("#frm :input").attr('disabled',false); 
    
    $.ajax({
            type: 'POST',
            url: $("#frm").attr('action'),
            data:$("#frm").serialize(),
            //dataType:'json',
            // Mostramos un mensaje con la respuesta de PHP
            success: function(data) {
                    MenuBar_EnableButtons(true);                    
                    MenuContext_EnableButtons(true);

                    accion_anterior = $('#accion').val();
                    
                    $("#div_formulario").empty();    
                    $('#div_formulario').html(data);                    

                    setComportamientoFormulario();
                    //Se oculta el loader
                    ViamaticaMensajeCargandoVisible(false);
                    //-----------------------------------------------------------------------------------------
            }
    })
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function 
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*
 *Cuando se realiza un cambio en uno de los campos del formulario se habilitar el 
 *boton grabar tanto en el MenuBar como en el MenuContextual
 **/
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function HabilitarBotonGrabar(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/    //Menu Superior
    MenuBar_EnableButton('principal','opciones','grabar',true);
    MenuContext_EnableButton('MenuContextual_Grabar', true);    
    $('#flagcambio').val('S');
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function HabilitarBotonGrabar
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Grabar_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    $('#frm').attr("action", baseUrl+'/contabilidad/plancuentas/mascaragrabar');
    
    if ($("#frm").validationEngine('validate')){
        submit_formulario();    
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Grabar_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Salir_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    if ($('#flagcambio').val()=='S'){
        jConfirm('Estas Seguro de Salir? !Tiene cambios que no ha grabado!','FibSys', function(r){
                                                    if(r){
                                                            window.close();	
                                                    }else{
                                                        return false;
                                                    }
        });//End Confirm
    }else{
       window.close(); 
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Salir_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------------------------------------------------------*/
function setComportamientoFormulario(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    switch($('#accion').val()){
        case 'M':  //Modificacion
            MenuBar_EnableButtons(true);                    
            MenuContext_EnableButtons(true); 
            MenuBar_EnableButton('principal','opciones','grabar',false);
            MenuContext_EnableButton('MenuContextual_Grabar', false);
            
            break;
    }//end switch

    $('#flagcambio').val('');   
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end functionsetComportamientoFormulario
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function agregarRow(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    var rowCount = $('#tbl_nivel tr').length;

    var control_nivel_eliminar_instance    = control_nivel_eliminar.replace(/id_row/g,rowCount);
    var control_nivel_secuencia_instance   = control_nivel_secuencia.replace(/id_row/g,rowCount);
    var control_nivel_longitud_instance    = control_nivel_longitud.replace(/id_row/g,rowCount);

    control_nivel_eliminar_instance        = control_nivel_eliminar_instance.replace(/plantilla/g,'arr');
    control_nivel_secuencia_instance       = control_nivel_secuencia_instance.replace(/plantilla/g,'arr');
    control_nivel_longitud_instance        = control_nivel_longitud_instance.replace(/plantilla/g,'arr');

    control_nivel_secuencia_instance       = control_nivel_secuencia_instance.replace(/value=""/,'value="'+rowCount+'"');


    $('#tbl_nivel').append('<tr>'
                        +'<td>'+control_nivel_eliminar_instance+'</td>'
                        +'<td>'+control_nivel_secuencia_instance+'</td>'
                        +'<td>'+control_nivel_longitud_instance+'</td>'
                     +'</tr>');

    HabilitarBotonGrabar(); 

    setComportamientoRowEliminar();
    
//    $("#frm").validationEngine('validate')      
    $('#frm').validate();
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function agregarRow
/*-------------------------------------------------------------------------------------------------------------------------------------*/

    
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function renumerar_secuencia(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    var i=1;
    $('.nivel_secuencia').each(function(index) {
        $(this).val(i);
        i++;
    });

    //grid_nivel_secuencia-1
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function renumerar_secuencia
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function mostrar_formato(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    var formato = '';
    $('.nivel_longitud').each(function(index) {
        formato = formato+$.repeat('x',$(this).val())+'-';
    });
    formato = formato.substr(0, formato.length - 1);
    $('#formato').val(formato);
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function mostrar_formato
/*-------------------------------------------------------------------------------------------------------------------------------------*/


/*
 *Cuando se realiza un cambio en uno de los campos del formulario se habilitar el 
 *boton grabar tanto en el MenuBar como en el MenuContextual
 **/
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function HabilitarBotonGrabar(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/    //Menu Superior
    MenuBar_EnableButton('principal','opciones','grabar',true);
    MenuContext_EnableButton('MenuContextual_Grabar', true);    
    $('#flagcambio').val('S');
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function HabilitarBotonGrabar
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Grabar_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    $('#frm').attr("action", baseUrl+'/contabilidad/plancuentas/mascaragrabar');
    
    if ($("#frm").validationEngine('validate')){
        submit_formulario();    
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Grabar_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/
