<?php

/**
 * Permite acceder a metodos comunes del esquema DAO
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage Respuesta
 */


class Viamatica_Respuesta
{    
    const CODIGO_OK         = '00';         const TEXTO_OK       = ' ';
    const CODIGO_NO_EXISTE  = '01';         const TEXTO_NO_EXISTE   = 'No existe registro';
    const CODIGO_NO_PROCESA = '02';         const TEXTO_NO_PROCESA  = 'Error no permite procesar';
    const CODIGO_ERROR      = '99';         //El texto del error se enviara el texto de la excepcion


}//end class
