    /*--------------------------------------------------*/
    /*function MenuBar_Nuevo_OnClick(){

        VentanasEmergentes(baseUrl+"/seguridad/cargo/abrirventana",
                           "popup_cargo_abrirventana","abrir","",
                           "width=1200,height=700,left=500,top=50,toolbar=NO,directories=NO,status=NO,menubar=NO,resizable=NO,scrollbars=NO");
    }//end function MenuBar_Nuevo_OnClick
    */


    /*--------------------------------------------------*/
    //Funcion Ready Jquery
    /*--------------------------------------------------*/
    $(document).ready(function()
    {	
        //Habilitar botones del Menu Principal
        /*PestanaInicial('principal');
        MenuBar_EnableButton('principal','opciones','nuevo',true);
        MenuBar_Button_EventClickUp('principal','opciones','nuevo',MenuBar_Nuevo_OnClick);*/
        
        
        //inicializa los los componentes de busqueda en el JqGrid
        $("#div_buscador_grid").hide(0);
        $("#div_buscador_grid_hide").hide(0);
        $("#div_buscador_grid_show ").show("fast");

        //Muestra el Componente de busqueda del JqGrid
        $("#mostrar_buscador").click(function(event){
            event.preventDefault();
            $("#div_buscador_grid_show").hide(0);
            $("#div_buscador_grid_hide").show("fast");
            $("#div_buscador_grid").show("fast");
        });

        //Oculta el Componente de busqueda del JqGrid
         $("#ocultar_buscador").click(function(event){
            event.preventDefault();
            $("#div_buscador_grid_show").show("fast");
            $("#div_buscador_grid_hide").hide(0);
            $("#div_buscador_grid").hide(0);
        });


    /*---------------------------------------------------*/
    //Codigo Propio de JqGrid
    /*---------------------------------------------------*/    
        jQuery("#GridBody").jqGrid({ 
            url:baseUrl+'/clientes/persona/grid', //la Url del Action que retorna el Objeto JSON 
            datatype: "JSON",
            mtype: 'POST',
            height: 'auto',            
            autowidth: true,
            colNames:[etiqueta_codigo,etiqueta_nombre,etiqueta_apellido,etiqueta_edad,etiqueta_sexo,etiqueta_pais,etiqueta_provincia,etiqueta_est_ecuatoriano,etiqueta_est_doble_nacionalidad,''],
            colModel:[ {name:'cod_persona',index:'cod_persona', hidden:false, width:40, align:'right'}, 
                       {name:'nom_persona',index:'nom_persona', width:80}, 
                       {name:'ape_persona',index:'ape_persona', width:80}, 
                       {name:'edad',index:'edad', width:30}, 
                       {name:'sexo',index:'sexo', width:30}, 
                       {name:'nom_pais',index:'nom_pais', width:60}, 
                       {name:'nom_provincia',index:'nom_provincia', width:60}, 
                       {name:'est_ecuatoriano',index:'est_ecuatoriano', width:90}, 
                       {name:'est_doble_nacionalidad',index:'est_doble_nacionalidad', width:90}, 
                       {name:'',index:''}],
            rowNum:jqgrid_limitdefault,
            rowList:jqgrid_limitrangos,
            pager: '#GridFoot',
            sortname: 'cod_persona', 
            viewrecords: true,
            sortorder: "asc",
            caption : etiqueta_clientes,
            loadError : function(xhr, st, str){$('#div_formulario').html(xhr.responseText);}
            //Permite el evento onclick del JqGrid
            
        }); 


        jQuery("#GridBody").jqGrid('navGrid','#GridFoot',{edit:false,add:false,del:false}); 

        //Se gradua el ancho del Grid en referencia con el buscador para que no haya desbordamiento
        $('#GridBody').autowidth = false;
        $('#GridBody').setGridWidth($('#GridBody').width() - $('#div_buscador_grid').width() - 10);
        
        
        $("#buscar").button();
        
        $('#buscar').click(function() {
            cargarGrid();
            return false;
        });        
        
    /*--------------------------------------------------*/
    });
    /*--------------------------------------------------*/                                             

    /*--------------------------------------------------*/
    //Permite interactuar con el buscador
    /*--------------------------------------------------*/
    function cargarGrid(){ 
        var cod_persona = jQuery("#cod_persona").val(); 
        var nom_persona = jQuery("#nom_persona").val(); 

        var urlConsulta = baseUrl+"/clientes/persona/grid/cod_persona/"+cod_persona+"/nom_persona/"+nom_persona;
        jQuery("#GridBody").jqGrid('setGridParam',{url:urlConsulta,page:1}).trigger("reloadGrid"); 
    /*---------------------------------------------------*/
    } //end function cargarGrid
    /*---------------------------------------------------*/
