/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*--------------------------------------------------------------------------*/
function cargarFormularioCompleto(urlForm, urlMenuBar, urlMenuIzquierdo){
/*--------------------------------------------------------------------------*/
    MenuBar_EnableButtons(false);                    
    jQuery(".formError").hide(); 
    
    ViamaticaMensajeCargandoVisible(true); 
    cargarFormularioMenuBar(urlMenuBar);
    if (urlMenuIzquierdo!=''){
        cargarFormularioMenuIzquierdo(urlMenuIzquierdo);
    }//end if
    //cargarFormularioMenuContext(urlMenuContext);
    cargarFormulario(urlForm);
}//end cargarFormularioCompleto
/*--------------------------------------------------------------------------*/

var httpR_Formulario;
/*--------------------------------------------------------------------------*/
function cargarFormulario(urlForm){
/*--------------------------------------------------------------------------*/   
    MenuBar_EnableButtons(false);                    
    //Oculta los mensajes de validacion del validationEngine()
    jQuery(".formError").hide(); 
    
    $("#div_formulario").empty();
    $.ajax({
        type: "POST",
        url: urlForm,
        cache:false,
        dataType: 'html',
        beforeSend: function(data2){
            if(httpR_Formulario){
                httpR_Formulario.abort();
            }
            httpR_Formulario = data2;
        },
        success: function(data){ 
            $("#div_formulario").html(data);
            ViamaticaMensajeCargandoVisible(false)    
        }
    });
}//end function cargarFormulario       
/*--------------------------------------------------------------------------*/

var httpR_MenuBar;
/*--------------------------------------------------------------------------*/
function cargarFormularioMenuBar(urlMenuBar){
/*--------------------------------------------------------------------------*/    
//    $("#div_menu_herramientas_popup").empty();
    $.ajax({
        type: "POST",
        url: urlMenuBar,
        cache:true,
        dataType: 'html',
        beforeSend: function(data2){
            if(httpR_MenuBar){
                    httpR_MenuBar.abort();
            }
            httpR_MenuBar = data2;
        },
        success: function(data){ 
             $("#div_menu_bar").html(data);
             $("#testRibbon").officebar({
                                        });
             
        }
    });
}//end function cargarFormularioMenuBar       
/*--------------------------------------------------------------------------*/

var httpR_MenuIzquierdo;
/*--------------------------------------------------------------------------*/
function cargarFormularioMenuIzquierdo(urlMenuIzquierdo){
/*--------------------------------------------------------------------------*/    
    $.ajax({
        type: "POST",
        url: urlMenuIzquierdo,
        cache:true,
        dataType: 'html',
        beforeSend: function(data2){
            if(httpR_MenuIzquierdo){
                    httpR_MenuIzquierdo.abort();
            }
            httpR_MenuIzquierdo = data2;
        },
        success: function(data){ 
             $("#div_content_menu_izquierdo").html(data);             
        }
    });
}//end function cargarFormularioMenuBar       
/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/
function cargarFormularioMenuContext(urlMenuContext){
/*--------------------------------------------------------------------------*/    
    //Oculta los mensajes de validacion del validationEngine()
/*    jQuery(".formError").hide(); 
    
    $("#div_formulario").empty();
    $.ajax({
        type: "POST",
        url: urlMenuBar,
        cache:false,
        dataType: 'html',
        success: function(data){ 
             $("#div_menu_bar").html(data);
        }
    });
*/
}//end function cargarFormularioMenuBar       
/*--------------------------------------------------------------------------*/