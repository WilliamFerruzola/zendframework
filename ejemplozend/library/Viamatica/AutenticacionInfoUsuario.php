<?php

/**
 * Permite acceder a los datos de la empresa
 * por medio del uso de webServices al servidor de negocio
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage AutenticacionInfoUsuario
 */


class Viamatica_AutenticacionInfoUsuario
{

    /**
     * Almacena el objeto Info del Usuario
     *
     * @var object
     */        
    private $_user = null;
    
    /**
     * Almacena el objeto de los accesos que tiene el usuario en la aplicacion
     *
     * @var object
     */        
    private $_accesos = null;
    
    
    /**
     * Obtiene la informacion del usuario
     * Vease los campos de la tabla usuario
     *
     * @return object
     */        
    public function getUser(){
        return $this->_user;
    }//end function getUser
    
    /**
     * Obtiene la informacion de los Accesos
     *
     * @return object
     */            
    public function getAccesos(){
        return $this->_accesos;
    }//end function getAccesos
    
     /**
      * Permite obtener la autentificacion de la informacion del Usuario 
      * realizada con el objeto ZEND_AUTH
      *
      * @return object|boolean
      */    
    public function __construct() {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->getIdentity();        
            $this->_user    = $auth->user;
            //$this->_accesos = $auth->accesos;    
        }//end if
    }//end function autentificacionInfoUsuarioGetData
    
}//end class

?>