<?php

/**
 * Permite generar la Barra de Menu Superior
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage MenuBarSuperior

 */
class Viamatica_MenuBarSuperior {
    /** @var string */    
    private $id = 'testRibbon';
    /** @var Array */    
    private $pestanas = null;
    /** @var string */

    const ICO_NUEVO                = 'icon_nuevo.png';
    const ICO_NUEVO_APAGADO        = 'icon_nuevo_apagado.png';
    const ICO_GRABAR               = 'icon_guardar.png';
    const ICO_GRABAR_APAGADO       = 'icon_guardar_apagado.png';
    const ICO_ELIMINAR             = 'icon_eliminar.png';
    const ICO_ELIMINAR_APAGADO     = 'icon_eliminar_apagado.png';
    const ICO_SALIR                = 'icon_salir.png';
    const ICO_SALIR_APAGADO        = 'icon_salir_apagado.png';
    const ICO_EXCEL                = 'icon_excel.png';
    const ICO_EXCEL_APAGADO        = 'icon_excel_apagado.png';
    const ICO_PDF                  = 'icon_pdf.png';
    const ICO_PDF_APAGADO          = 'icon_pdf_apagado.png';
    const ICO_XML                  = 'icon_xml.png';
    const ICO_XML_APAGADO          = 'icon_xml_apagado.png';
    const ICO_LISTADO              = 'icon_listado.png';
    const ICO_LISTADO_APAGADO      = 'icon_listado_apagado.png';
    const ICO_EDITAR               = 'icon_editar.png';
    const ICO_EDITAR_APAGADO       = 'icon_editar_apagado.png';
    
    
    
    /**
     * Obtiene Id del MenuBar
     * 
     * @return String
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param String $id 
     */
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * Obtiene las pestanas 
     * adicionalmente se anexa los paneles y los botones, a continuacion describimos
     * la estructura:
     * 
     * $this->pestanas[idPestana]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[idPestana]->panel[idPanel]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[idPestana]->panel[idPanel]->boton[idBoton]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[]   (Obtiene todas las pestanas)
     * 
     * $this->pestanas[idPestana]->panel[]   (Obtiene todas los paneles de una pestana)
     * 
     * $this->pestanas[idPestana]->panel['idPanel]->boton[]   (Obtiene todas los botones de una pestana)
     * 
     * @return StdClass[]
     */
    public function getPestanas(){
        return $this->pestanas;
    }
    
    public function addPestana($id_pestana, $titulo, $default = false){
        $pestana = new StdClass();
        
        $pestana->id = $id_pestana;
        $pestana->titulo = $titulo;
        $pestana->default = $default;
        
        $this->pestanas["pestana_$id_pestana"] = $pestana;
    }

    public function addPanel($id_pestana, $id_panel, $titulo){
        $panel = new StdClass();
        
        $panel->id = $id_panel;
        $panel->titulo = $titulo;
        
        $this->pestanas["pestana_$id_pestana"]->panel["panel_$id_panel"] = $panel;
    }
    
    public function addBoton($id_pestana, $id_panel, $id_boton, $titulo, $icono_activo, $icono_apagado){
        $boton = new StdClass();
        
        $boton->id              = $id_boton;
        $boton->titulo          = $titulo;
        $boton->icono_activo    = $icono_activo;
        $boton->icono_apagado   = $icono_apagado;

        $this->pestanas["pestana_$id_pestana"]->panel["panel_$id_panel"]->boton["boton_$id_boton"] = $boton;
    }
    
    public function getMenuBar(Viamatica_MenuBarSuperior $data){
        $translator = Zend_Registry::get('Zend_Translate');
        
        //Ubico la ruta base utilizando el Helper de la Vista
        $helper_view = new Zend_View_Helper_BaseUrl();
        $BaseUrl  = $helper_view->baseUrl();

        if(!Zend_Auth::getInstance()->hasIdentity()) {
            return false;
        }//end if

        $html ='<div id="testRibbon" class="officebar" style="position:relative;">'
              .'<ul>';

        //Se define los paneles
        foreach($data->getPestanas() as $regPestana){
            // html.='<li id="'.$regPestana->id_pestana." class="current">
            if ($regPestana->default){
                $html.='<li id="li_'.$regPestana->id.'" class="current">'
                      .'<a href="#" rel="home">'.$translator->_($regPestana->titulo).'</a>'
                      .'<ul>';
            }else{
                $html.='<li id="li_'.$regPestana->id.'">'
                      .'<a href="#">'.$translator->_($regPestana->titulo).'</a>'
                      .'<ul>';
            }

            foreach($regPestana->panel as $regPanel){
                $html.='<li id="li_'.$regPestana->id.'_'.$regPanel->id.'">'
                      .'    <span>'.$translator->_($regPanel->titulo).'</span>';
                 foreach($regPanel->boton as $regBoton){
                     $html.='<div class="button menubarSuperior_boton_activo" id="div_'.$regPestana->id.'_'.$regPanel->id.'_'.$regBoton->id.'" style="display:none">'
                           .'  <a href="#" id="link_'.$regPestana->id.'_'.$regPanel->id.'_'.$regBoton->id.'" >'
                           .'     <span>'.$translator->_($regBoton->titulo).'</span><img src="'.$BaseUrl.'/images/general/'.$regBoton->icono_activo.'"  alt=""/>'
                           .'  </a>'
                           .'</div>';

                     $html.='<div class="button menubarSuperior_boton_inactivo" id="div_'.$regPestana->id.'_'.$regPanel->id.'_'.$regBoton->id.'_apagado">'
                           .'  <a href="#" id="link_'.$regPestana->id.'_'.$regPanel->id.'_'.$regBoton->id.'" >'
                           .'     <span>'.$translator->_($regBoton->titulo).'</span><img src="'.$BaseUrl.'/images/general/'.$regBoton->icono_apagado.'"  alt=""/>'
                           .'  </a>'
                           .'</div>';

                 }//endforeach
                $html.='</li>';

            }//end foreach panel
            $html.='</ul>'
                  .'</li>';
        }//end foreach

        $html.='</ul>'
              .'</div>';

        unset($translator);
        unset($helper_view);
        unset($BaseUrl);

        return $html;
        
    }//end getMenuBar
    
    
}//end class

?>
