<?php

/**
 * Permite manejar las excepciones de una forma profesional y ordenada
 * el cual permitirá grabar dichas excepciones en log, los cuales estarán ubicados
 * en la carperta /logs/log-aaaa-mm-dd.txt
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage Exception
 */


class Viamatica_Exception extends Exception
{
    private static $logger          = NULL;
    private $log_mensajeCompleto    = NULL;
    private $log_mensajeSimple      = NULL;
    private $log_arr                = Array();

   /**
    * Registra la excepcion hacia los logs de forma formateada
    * 
    * @param Exception  $e
    * @param int        $nivel     Utiliza la clase Log 
    */ 
   public function registrar(Exception $e, $nivel)
    {
        if (is_null(self::$logger)){
            self::$logger = Zend_Registry::get('logger');
        }//end if
       
        $this->grabarMensaje($e, $nivel);
        $this->grabarTrace($e->getTrace(), $nivel);
        
        //Si en el config.ini el manejo de excepcion de la capa de negocio
        //esta deshabilitado lo omite para pasarlo en el webservices
        $config 		= Zend_Registry::get('config');
        if ($config->ws->exception==0){
            $this->log_arr = Array();
        }//end if
    }//end static function

    /**
     *
     * @param Exception $e
     * @param int       $nivel      Utiliza la clase Log
     */
    private function grabarMensaje($e, $nivel){
        $cadena  = str_pad('', 100, '-');
        //Viamatica_UTF8::convertir($cadena);
        self::$logger->$nivel($cadena);
        $this->log_arr[]=$cadena;

        $cadena = '(log_mensaje) '.$e->getMessage().' in '.$e->getFile().' on line '.$e->getLine();      
        //Viamatica_UTF8::convertir($cadena);        
        self::$logger->$nivel($cadena);
        $this->log_arr[]=$cadena;      
        $this->log_mensajeCompleto  = $cadena;
        //Viamatica_UTF8::convertir($this->log_mensajeCompleto);        
        $this->log_mensajeSimple    = $e->getMessage();
        //Viamatica_UTF8::convertir($this->log_mensajeSimple);

        $cadena  = str_pad('', 100, '-');
        //Viamatica_UTF8::convertir($cadena);        
        self::$logger->$nivel($cadena);
        $this->log_arr[]=$cadena;        
        
        unset($cadena);        
    }//end function grabarMensaje
    
    /**
     * Graba la traza
     * 
     * @param Array $trace  Trace de la Exception
     * @param int   $nivel  Utiliza la clase Log
     */
    private function grabarTrace($trace, $nivel){
        $obj = new stdClass();

        foreach($trace as $reg){
            $cadena = $reg['class'].$reg['type'].$reg['function'].'(';
            $bd_parametro = false;
            
            $arr_parametro = null;
            foreach($reg['args'] as $parametros){
                $bd_parametro = true;
                if (is_object($parametros)){
                    $arr_parametro[] = $parametros;
                    $cadena.= (String)get_class($parametros).',';
                }else{
                    $cadena.=$parametros.',';                
                }
            }//end foreach
            
            if ($bd_parametro){
                $cadena = substr($cadena,0,strlen($cadena)-1);
            }//end if
            
            $cadena.= ')';
            $cadena = str_pad($cadena, 75);
            $cadena.= ' '.$reg['file'].':'.$reg['line'];
            //Viamatica_UTF8::convertir($cadena);            
            self::$logger->$nivel($cadena);
            $this->log_arr[]=$cadena;
            
            if (is_array($arr_parametro)){
                foreach ($arr_parametro as $var => $value){
                    $cad_parametro = str_pad(' ',5).'(PARAMETRO OBJETO =>'.(String)get_class($value).')';
                    //Viamatica_UTF8::convertir($cad_parametro);                
                    self::$logger->$nivel($cad_parametro); 
                    $this->log_arr[]=$cad_parametro;
                    $this->grabarParametros($value, $nivel, 2);
                }//end foreach
            }//end if
        }//end foreach
        
        self::$logger->$nivel('');
        $this->log_arr[]='';
        unset($cadena);
        unset($obj);
    }//end function grabarTrace

    /**
     * Graba los parametro tipo Objetos
     * 
     * @param Mixed $obj
     * @param int   $nivel          Utiliza la clase Log
     * @param int   $margenDerecho  Se pasa el nivel del margen derecho el cual será multiplicado internamente por CINCO 
     */
    private function grabarParametros($obj, $nivel, $margenDerecho){
        foreach ($obj as $var => $value ){
            if (!is_object($value)){
                $cad_parametro = str_pad(' ',$margenDerecho*5).$var.'=>'.$value;
                //Viamatica_UTF8::convertir($cad_parametro);
                self::$logger->$nivel($cad_parametro);
                $this->log_arr[]=$cad_parametro;
            }else{
                $cad_parametro = str_pad(' ',$margenDerecho*5).$var.'=>'.(String)get_class($value);
                //Viamatica_UTF8::convertir($cad_parametro);                
                self::$logger->$nivel($cad_parametro);
                $this->log_arr[]=$cad_parametro;
                self::grabarParametros($value, $nivel, $margenDerecho+1);
            }//end if
        }//end foreach
    }//end function grabarParametros
    
    /**
     * Obtiene el Log en un Array
     * 
     * @return Array 
     */
    public function obtenerLogArray(){
        return $this->log_arr;
    }//end function obtenerString

    /**
     * Obtiene el mensaje del error, pero sin la traza
     * en el que se indicara el archivo y linea donde esta el error
     * 
     * @return String
     */
    public function obtenerLogMensajeCompleto(){
        return $this->log_mensajeCompleto;
    }//end function obtenerLogMensajeCompleto

    /**
     * Obtiene el mensaje del error, pero sin la traza
     * en el que se indicará solamente el mensaje sin especificar el archivo 
     * ni la linea donde se encuentra el error
     * 
     * @return String
     */
    public function obtenerLogMensajeSimple(){
        return $this->log_mensajeSimple;
    }//end function obtenerLogMensajeSimple
    
}//end class