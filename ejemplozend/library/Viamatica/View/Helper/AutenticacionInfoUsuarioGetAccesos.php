<?php

class Viamatica_View_Helper_AutenticacionInfoUsuarioGetAccesos
{

   
     /**
     * idiomaSistema
     *
     * @return String
     */    
    public function autenticacionInfoUsuarioGetAccesos()
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->getIdentity()->accesos;        
            return $auth;
        }//end if
      }
    
}
