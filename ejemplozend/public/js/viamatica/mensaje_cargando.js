
/*
 * Permite mostrar o ocultar el mensaje de cargando los cuales puede aceptar
 * los siguientes valores:
 * 
 *      true  => mostrar
 *      false => ocultar
 */
function ViamaticaMensajeCargandoVisible(visible)
{
    if (visible){
        $('#div_cargando').show()
    }else{
        $('#div_cargando').hide()
    }
}//end function ViamaticaMensajeCargandoVisible

