<?php

/**
 * Permite generar la Barra de Menu Superior
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica
 * @subpackage MenuContext

 */
class Viamatica_MenuContext {
    /** @var Array */    
    private $acciones = null;

    const ICO_NUEVO                = 'icon_nuevo.png';
    const ICO_NUEVO_APAGADO        = 'icon_nuevo_apagado.png';
    const ICO_GRABAR               = 'icon_guardar.png';
    const ICO_GRABAR_APAGADO       = 'icon_guardar_apagado.png';
    const ICO_ELIMINAR             = 'icon_eliminar.png';
    const ICO_ELIMINAR_APAGADO     = 'icon_eliminar_apagado.png';
    const ICO_SALIR                = 'icon_salir.png';
    const ICO_SALIR_APAGADO        = 'icon_salir_apagado.png';
    
    
    /**
     * Obtiene las pestanas 
     * adicionalmente se anexa los paneles y los botones, a continuacion describimos
     * la estructura:
     * 
     * $this->pestanas[idPestana]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[idPestana]->panel[idPanel]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[idPestana]->panel[idPanel]->boton[idBoton]    (Obtiene una pestana en particular)
     * 
     * $this->pestanas[]   (Obtiene todas las pestanas)
     * 
     * $this->pestanas[idPestana]->panel[]   (Obtiene todas los paneles de una pestana)
     * 
     * $this->pestanas[idPestana]->panel['idPanel]->boton[]   (Obtiene todas los botones de una pestana)
     * 
     * @return StdClass[]
     */
    public function getAcciones(){
        return $this->acciones;
    }

    public function addAccion($id_accion, $titulo, $icono_activo, $icono_apagado){
        $accion = new StdClass();
        
        $accion->id              = $id_accion;
        $accion->titulo          = $titulo;
        $accion->icono_activo    = $icono_activo;
        $accion->icono_apagado   = $icono_apagado;

        $this->acciones[$id_accion] = $accion;
    }
    
    
}

?>
