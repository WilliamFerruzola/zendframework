<?php

/**
 * Permite tener una representacion de la informacion de los modulos,
 * opciones y acciones como se encuentra en la base de datos
 * con la finalidad de poder optimizar el tráfico de los WebServices
 *
 * @author David Salazar
 * @version 1.0
 * @package Viamatica_Data
 * @subpackage Modulos

 */
class Viamatica_Data_Modulos{

    /** @var Array $modulos */    
    private static $modulos = null;

    
    //Constante para los codigos de los modulos
    const COD_MODULO_SEGURIDAD     = 1;
    const COD_MODULO_CONTABILIDAD  = 2;
    const COD_MODULO_BANCOS        = 3;

    //Constantes para los codigos de las acciones
    const COD_ACCION_INGRESAR      = 1;
    const COD_ACCION_MODIFICAR     = 2;
    const COD_ACCION_ELIMINAR      = 3;
    const COD_ACCION_CONSULTAR     = 4;
     
    /**
     * permite obtener la data
     * @return Array
     */
    public static function getData(){
        if (empty(self::$modulo)){
            self::cargarDatos();
        }//end if
         return self::$modulos;
    }
    
    /**
     * Permite agregar un modulo al objeto
     * 
     * @param integer $id_modulo
     * @param String  $titulo
     */
    public static function addModulo($id_modulo, $titulo, $ruta){
        $modulo = new StdClass();
        
        $modulo->id = $id_modulo;
        $modulo->titulo = $titulo;
        $modulo->ruta = $ruta;

        self::$modulos[$id_modulo] = $modulo;
    }


    /**
     * Permite agregar las opciones a los modulos
     * 
     * @param integer $id_modulo
     * @param integer $id_opcion
     * @param String $titulo
     * @param String $ico
     * @param String $acciones 
     */
    public static function addOpcion($id_modulo, $id_opcion, $titulo, $ruta, $menubar, $ico, $acciones){
        $opcion = new StdClass();
        
        $opcion->id         = $id_opcion;
        $opcion->titulo     = $titulo;
        $opcion->ruta       = $ruta;        
        $opcion->menubar    = $menubar;        
        $opcion->ico        = $ico;
        $opcion->acciones   = $acciones;

        self::$modulos[$id_modulo]->opcion[$id_opcion] = $opcion;
    }
    
    
    /**
     * Permite cargar la Data 
     * Simulando a la estructura y registros de la base de datos, pero orientado a objetos
     */
    private static function cargarDatos(){
        self::addModulo(self::COD_MODULO_SEGURIDAD, 'Seguridad y Parámetros','/seguridad');
        self::addModulo(self::COD_MODULO_CONTABILIDAD, 'Contabilidad','/contabilidad');
        self::addModulo(self::COD_MODULO_BANCOS, 'Bancos','/bancos');
        self::addModulo(self::COD_MODULO_BANCOS, 'Facturacion','/facturacion');

        //SEGURIDAD
        self::addOpcion(self::COD_MODULO_SEGURIDAD,1,'Empresa','/empresa/listado','/empresa/cargarmenubarprincipal',Viamatica_Icono::ICO_EMPRESA, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,2,'Sucursal','/sucursal/listado','/sucursal/cargarmenubarprincipal',Viamatica_Icono::ICO_SUCURSAL, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,3,'Usuario','/usuario/listado','/usuario/cargarmenubarprincipal',Viamatica_Icono::ICO_USUARIO, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,4,'Perfiles','/perfil/listado','/perfil/cargarmenubarprincipal',Viamatica_Icono::ICO_PERFILES, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,5,'Asignacion Perfiles','/asignacionperfil','',Viamatica_Icono::ICO_ASIGNACION, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,6,'Permisos Especiales','/permisosespeciales','',Viamatica_Icono::ICO_PERMISOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,7,'Log Accesos','/logaccesos','',Viamatica_Icono::ICO_LOG_ACCESOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,8,'Log Sistema','/logsistema','',Viamatica_Icono::ICO_LOG_SISTEMA, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,9,'Documentos','/documentos','',Viamatica_Icono::ICO_DOCUMENTOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,10,'Servidores','/servidores','',Viamatica_Icono::ICO_SERVIDORES, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,11,'Parametros','/parametros','',Viamatica_Icono::ICO_PARAMETROS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,12,'Cargo','/cargo/listado','/cargo/cargarmenubarprincipal',Viamatica_Icono::ICO_EMPRESA, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_SEGURIDAD,13,'Departamento','/departamento/listado','/departamento/cargarmenubarprincipal',Viamatica_Icono::ICO_EMPRESA, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');

        //CONTABILIDAD
        self::addOpcion(self::COD_MODULO_CONTABILIDAD,1,'Definicion Formatos','/definicionformatos','',Viamatica_Icono::ICO_FORMATOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_CONTABILIDAD,2,'Plan de Cuentas','/plancuentas','',Viamatica_Icono::ICO_PLANCUENTA, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_CONTABILIDAD,3,'Formato Plan Cuenta','/mascaraconsultar','',Viamatica_Icono::ICO_FORMATOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');
        self::addOpcion(self::COD_MODULO_CONTABILIDAD,4,'Cotizacion de Moneda','/tipocambio/listado','/tipocambio/cargarmenubarprincipal',Viamatica_Icono::ICO_FORMATOS, self::COD_ACCION_INGRESAR.'|'.self::COD_ACCION_MODIFICAR.'|'.self::COD_ACCION_ELIMINAR.'|'.self::COD_ACCION_CONSULTAR.'|');

    }//end function
        
}//end class

?>
