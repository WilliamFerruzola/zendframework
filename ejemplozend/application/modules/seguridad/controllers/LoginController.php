<?php
require_once 'seguridad/models/LoginModelo.php';

class Seguridad_LoginController extends Zend_Controller_Action{
    
/*---------------------------------------------------------------------------------------*/
    public function init(){
/*---------------------------------------------------------------------------------------*/	
 //       $this->translate = Zend_Registry::get("translate");        
/*---------------------------------------------------------------------------------------*/
    }//end function init()
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
    public function preDispatch(){
/*---------------------------------------------------------------------------------------*/	
/*        if(!LoginModelo::estaLogueado()) {
            $this->view->loggedIn = false;
        } else {
            $this->view->loggedIn = true;
        }
 */
/*---------------------------------------------------------------------------------------*/
    }//end function
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
    public function indexAction(){
/*---------------------------------------------------------------------------------------*/
 //       $this->_helper->viewRenderer->setNoRender(true); //Desactiva la Vista
        $this->_helper->layout->disableLayout(); // Desactiva el Layout
        
//        $this->view->usuario = LoginModelo::getIdentidad();
//        $this->view->render("autenticar.phtml");
/*---------------------------------------------------------------------------------------*/		
    }//end function
/*---------------------------------------------------------------------------------------*/	
	

/*---------------------------------------------------------------------------------------*/
    public function autenticarAction(){
/*---------------------------------------------------------------------------------------*/	
       // $this->_helper->viewRenderer->setNoRender(true); //Desactiva la Vista
        $this->_helper->layout->disableLayout(); // Desactiva el Layout
        
        $this->_login = new LoginModelo();

        $session_translate = new Zend_Session_Namespace('translate');        
        
        $session_translate = $session_translate;
        $this->view->etiquetas = $session_translate->idioma; //$this->translate->_("usuario");

        if ($this->getRequest()->isPost()){
            //    return $this->_forward('index');
            $request = $this->getRequest();
            $email = $request->getParam('txt_user');
            $clave = $request->getParam('txt_clave');

            try {
                $this->_login->login($email, $clave);

    /*----*/
                $auth = Zend_Auth::getInstance();
                $autenticacion =	LoginModelo::getIdentidad();

                //Asignamos valores a la session del usuario
                $session_usuario = new Zend_Session_Namespace('usuario');
                $session_usuario->cod_usuario 	= $autenticacion->cod_usuario;
                $session_usuario->usr_name 	= $autenticacion->usr_name;
                $session_usuario->nombre        = $autenticacion->nombre;		
                $session_usuario->cargo         = $autenticacion->cargo;		
                
                //Zend_Debug::dump($session_usuario->nombre);
                
                $this->_redirect('clientes/index/listado');//no tiene accesos se destruye la session del usuario
                
    /*---*/
            } catch (Exception $ex) {
                    echo ($ex->getMessage());
                    $this->view->mensaje = $ex->getMessage();
            }//end try
        }//end if
/*---------------------------------------------------------------------------------------*/		
    }//end function autenticarAction
/*---------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------*/
    public function logoutAction(){
/*---------------------------------------------------------------------------------------*/	
        $this->_login->logout();
        $this->_redirect("");
/*---------------------------------------------------------------------------------------*/
    }//end function logoutAction
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
	public function abriraplicacionAction(){
/*---------------------------------------------------------------------------------------*/
		$this->_helper->layout()->setLayout($this->_config->propertiesLayout->layout);
					
/*---------------------------------------------------------------------------------------*/
	}//end function abriraplicacionAction
/*---------------------------------------------------------------------------------------*/


}//end class Seguridad_LoginController 