//*******************************************************************************************************************
// Estos son los JavaScripts de La barra superior que contiene botones de informacion
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
//*******************************************************************************************************************	
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
//*******************************************************************************************************************	
	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}
//*******************************************************************************************************************
	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
			
//*******************************************************************************************************************
	//Esta funcion exporta a Excel 
 	function exportExcel(raiz,cabecera,cuerpo)
    {
		//location.href=raiz+"/exports/excel/ExportExcel.php?bodytext="+cuerpo+"&cabeceratext="+cabecera;
		window.open(raiz+"/exports/excel/ExportExcel.php?bodytext="+cuerpo+"&cabeceratext="+cabecera);
		location.href = location.href; 		 
    }
//*******************************************************************************************************************
	//Esta funcion exporta a Pdf
 	function exportPdf(raiz,cabecera,cuerpo)
    {
		//location.href=raiz+"/exports/pdf/ExportPdf.php?bodytext="+html+"&cabeceratext="+cabecera;
		window.open(raiz+'/exports/pdf/ExportPdf.php?bodytext='+cuerpo+'&cabeceratext='+cabecera);
		location.href = location.href; 
    }
//*******************************************************************************************************************
	//Esta funcion exporta a Xml
 	function exportXml(raiz,cabecera,cuerpo)
    {
		//location.href=raiz+"/exports/xml/ExportXml.php?bodytext="+html+"&cabeceratext="+cabecera;
		window.open(raiz+'/exports/xml/ExportXml.php?bodytext='+cuerpo+'&cabeceratext='+cabecera);
		location.href = location.href; 
    }
//*******************************************************************************************************************
	//Esta funcion permite setear el check a un radio button
	function CheckedRdb(id)
	{		
		$('#'+id).attr('checked','checked');
	}
//*******************************************************************************************************************
	//Esta funcion permite deshabilitar los inputs
	function deshabilitar_input(id)
	{		
		$('#'+id).attr('disabled','disabled');
	}

//*******************************************************************************************************************	
	//Solo Numeros en los Textbox
	function soloNumero(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
		return true;
	}		

//*******************************************************************************************************************	
	//Solo Letras en los Textbox
	function soloLetra(e) 
	{
		key = (document.all) ? e.keyCode : e.which;
		if (key==8) return true;
		if (key==32) return true;
		patron =/[A-Za-z]/;
		te = String.fromCharCode(key);
		return patron.test(te);
	}	
	
//*******************************************************************************************************************	
	function validaEvento(e){
		var keynum;
		if(window.event){ keynum = e.keyCode; }
		else if(e.which){ keynum = e.which;   }
		return keynum;
	}
	
//*******************************************************************************************************************	
	function soloNumeroLetra(evento){
		//window.event.keyCode valor ascii de tecla pulsada
		var key = validaEvento(evento);
		
		if (key==8) return true;
		if (key==32) return true;
		
		if ((key < 48 || key > 57)){                          
					   if ((key <65  || key > 90)&&(key <97  || key > 122)){                                     
									   if(window.event){ 
													   evento.keyCode = 0;
													   respuesta = false;
									   }
									   else if(evento.which){                                 
													   return false;
									   }//end if window.event
					   }//end if key < 65
		}              //end if key < 48
	}
	
//*******************************************************************************************************************	

                
//*******************************************************************************************************************	
	//Funcion Permite Manipular las ventanas emergentes la pantalla en particular
	var VentanasHijas  = new Array();
	function VentanasEmergentes(pagina,id,accion,especificacion,atributos){
		//pagina: location de la pagina
		//id : id de la pagina o nombre
		//accion: abrir o cerrar
		//especificacion: cerrar todos o especifica
		//atributos: se ingresan los atribotus del la nueva pag
		//-----------------------------------------------------------------------	
		if(accion=='abrir'){
			e = false
			for(t = 0; t < VentanasHijas.length; t++){
				if(VentanasHijas[t]== id){
					e = true;
					break;
				}
			}
			eval(id+"=window.open(pagina,'" + id + "',atributos)")
			eval(id).focus() 
			if(!e){
				VentanasHijas[VentanasHijas.length] = id
			}
		}else{//cerrar
			if(especificacion=='todas'){
				for(m=0;m<VentanasHijas.length;m++){
				  if(eval(VentanasHijas[m])){
					eval(VentanasHijas[m]).close()
					}
				  }
				VentanasHijas.length = 0
			}else{
			
				for(m=0;m<VentanasHijas.length;m++){
				  if(eval(VentanasHijas[m])){
					  if(VentanasHijas[m]==id){
						eval(VentanasHijas[m]).close()
					   }
					}
				  }
			}
		}//end if
	}//end function 

//*******************************************************************************************************************	

