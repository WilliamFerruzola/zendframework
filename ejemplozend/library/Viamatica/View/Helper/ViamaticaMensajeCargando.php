<?php

/**
 * Muestra el mensaje de Cargando esto es muy util para
 * poder interactuar con los Ajax
 *
 * Debe de utilizar el javascript que se encuentra en public/js/viamatica/mensaje_cargando.js
 *
 * @author David Salazar
 * @version 1.0
 * @package Zend_View_Helper
 * @subpackage ViamaticaMensajeCargando
 */

class Zend_View_Helper_ViamaticaMensajeCargando extends Zend_View_Helper_Abstract{

    //-------------------------------------------------------------------------------------------------------------//
    public function ViamaticaMensajeCargando()
    {
      $helper_view = new Zend_View_Helper_BaseUrl();
      $UrlBase = $helper_view->baseUrl();

      $html='<script>'
             .' $(document).ready(function(){'
             .'   $("#div_cargando_texto").blink(500); '
             .'});'
             .'</script>'
             .'<div id="div_cargando" style="display:none;position:absolute;top: 50%;left: 50%;height:120px;width:150px;margin-top:-30px;margin-left:-0px">'
             .'   <div style="color: green; text-align:center; font-family:Arial; font-weight:bold; font-size:14px;top: 50%;left: 50%">'
             .'       <div id="div_cargando_texto">Procesando...</div>'
             .'   </div>'
             .'   <img src="'.$UrlBase.'/images/general/loader_carga.gif" height="100" />'
             .'</div>';

        unset($helper_view);
        unset($UrlBase);

        return $html;
    }//end function ViamaticaMensajeCargando()

}//end class

?>