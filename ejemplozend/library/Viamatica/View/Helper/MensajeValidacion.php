<?php

class Viamatica_View_Helper_MensajeValidacion 
{
    const INDICADOR_ERROR  = 'error';
    const INDICADOR_ALERTA = 'alerta';
    const INDICADOR_INFO   = 'info';
    const INDICADOR_EXITO  = 'exito';

    private function construirMensajeValidacion($data){
        $html = '';
        foreach($data as $key => $value){
           // echo("<br><br>key:".$key."*value:".$value);
            foreach ($value as $key2 => $value2){
               $html = $html. '<li style="margin-left:10px">'.$value2.'</li>';
            }//end foreach
        }//end foreach
        return $html;
    }//end function construirMensajeValidacion

    
    private function construirMensajeEjecucion($data){
       $html = '<li style="margin-left:10px">'.$data.'</li>';
       return $html;
    }//end function construirMensajeValidacion
    
   
     /**
     * mensajeValidacion
     *
     * @param Array $info form, ejecucion
     * @param integer $ancho
     * @param String $indicador; 
     * @return String
     */    
    public function mensajeValidacion($info, $ancho, $indicador)
    {
        if (!$indicador){
            $indicador = Viamatica_View_Helper_MensajeValidacion::INDICADOR_ALERTA;
        }//end if
        $html = "";
        if ((!empty($info['form']))||(!is_null($info['ejecucion']))){
            $html = '<div id="mensaje" class="'.$indicador.'" style="width:'.$ancho.'px">';
            if ($info['form']){
                $html = $html.$this->construirMensajeValidacion($info['form']);                
            }//end if
            if ($info['ejecucion']){
                $html = $html.$this->construirMensajeEjecucion($info['ejecucion']);
            }//end if            
            $html = $html.'</div>';    
        }//end if
        return $html;        
      }
    
}
