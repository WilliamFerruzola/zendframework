<?php
require_once "Zend/Loader/Autoloader.php";

/*Zend_Loader::loadClass("Zend_Config_Ini");
Zend_Loader::loadClass("Zend_Registry");
Zend_Loader::loadClass("Zend_Controller_Front");
Zend_Loader::loadClass("Zend_Layout");
Zend_Loader::loadClass('Zend_Date');
Zend_Loader::loadClass('Zend_Session');
*/

final class Bootstrap
{
    private static $_baseDir = null;


    public static function main()
    {
		//Con esta linea nidicamos de forma explicita el uso de las variables de sesion
	   	//Zend_Session::start();
		
        $loader = Zend_Loader_Autoloader::getInstance();
        $loader->setFallbackAutoloader(true);
        $loader->suppressNotFoundWarnings(false);

        self::inicializaConfiguracion();
        
        self::inicializaConexion();

        self::inicializaLenguaje();

        self::inicializaLayout();

        self::inicializaEntorno();
        
        self::inicializaDebug();
        
        self::inicializaFrontController();
        

    }
    
    
    /**
     * Carga el archivo de configuración INI, el archivo que contiene todos
     * los datos de configuración de nuestro sistema, como por ejemplo layout,
     * información del timezone, textos, etc.
     * @return Zend_Config_Ini archivo de configuraciónm de la aplicación
     */
    public static function inicializaConfiguracion()
    {        
        $config = new Zend_Config_Ini("config.ini", "default");
        Zend_Registry::set("config", $config);
        return $config;
    }
    
    
    /**
     * Contiene las operaciones de configuración relacionado con el
     * manejo de errores y el Timezone, que corresponde al Locale
     * de la fecha del sistema.
     */
    public static function inicializaEntorno()
    {
        error_reporting(E_ALL | E_STRICT);

        try{
            if ( !(String)Zend_Registry::get("config")->parametro->timezone ) {
                throw new Exception('Debe definir el timeZone en su archivo de configuración.');
            }else{
                $timeZone = (String)Zend_Registry::get("config")->parametro->timezone;
                date_default_timezone_set($timeZone);
                //echo(date("Y-m-d H:i:s"));
            }
        } catch (Exception $e) {
            echo $e->getTrace();
            exit;
        }

    }

   public static function inicializaDebug()
   {

   }
   public static function inicializaConexion()
    {
        $config = Zend_Registry::get("config");

        try {
            $db = Zend_Db::factory($config->database);
            $conexion = $db->getConnection();
            Zend_Db_Table::setDefaultAdapter($db);
            Zend_Registry::set('db', $db);
        } catch (Zend_Db_Adapter_Exception $ex) {
            throw new Zend_Db_Adapter_Exception("Mensaje:  " . $ex->getMessage(). "\n");
        } catch (Zend_Exception $ex) {
            throw new Zend_Exception("Mensaje:  " . $ex->getMessage(). "\n");
        }

        return $db;
    }
    
    
    /**
     * Inicializa el componente Zend Layout. Notamos que obtiene desde el
     * objeto de configuración INI los parámetros de configuración,
     * como la ruta del layout y su nombre. Luego asignamos el titulo
     * como variable.
     * @return Zend_Layout layout de la aplicación
     */
    public static function inicializaLayout()
    {
        $config = Zend_Registry::get("config");
        $layout = Zend_Layout::startMvc($config->propertiesLayout);
        //$layout->titulo = $config->titulo->aplicacion;
        return $layout;
    }

    /**
     * Encargado de iniciar el Front Controller y despachar las peticiones (Dispatch).
     */
    public static function inicializaFrontController()
    {

        $frontController = Zend_Controller_Front::getInstance();

        $frontController->addModuleDirectory(self::getRootApp() . "/modules");
			
        //Establecemos que se muestren las exceptiones (IMPORTANTE: PONER A FALSE EN PRODUCCION)
        $frontController->throwExceptions(true);

        $frontController->dispatch();
    }

    
    /**
     * Establece el root path de la aplicación.
     * @param String $dir Root path de la aplicación
     */
    public static function setBaseDir($dir)
    {
        self::$_baseDir = $dir;
    }

    
    /**
     * Devuelve el root path de la aplicación.
     * @return String El root path de la aplicación
     */
    public static function getBaseDir()
    {
        return self::$_baseDir;
    }

    
    /**
     * Devuelve el directorio donde se encuntra la aplicación.
     * @return String Ruta de la aplicación
     */
    public static function getRootApp()
    {
        return self::getBaseDir() . DIRECTORY_SEPARATOR . "application";
    }
    
	
    public static function inicializaLenguaje(){
        $config = Zend_Registry::get("config");
        $session_translate = new Zend_Session_Namespace('translate');             
        if (!isset($session_translate->idioma)){
            $translate = new Zend_Translate('csv','../application/lang/'.$config->idioma->defecto.'.csv',$config->idioma->defecto);
            // Zend_Registry::set("translate", $translate);	
            $session_translate->idioma = $translate;
            return $translate;              
        }
    }

	
	
    
}