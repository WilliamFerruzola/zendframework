<?php
//error_reporting(0);
//ini_set('display_errors',0);
ini_set('max_execution_time',0);


$rootPath = dirname(dirname(__FILE__));

$paths = get_include_path();
$paths = $paths .PATH_SEPARATOR.$rootPath . '/application';
$paths = $paths .PATH_SEPARATOR.$rootPath . '/application/modules';
$paths = $paths .PATH_SEPARATOR.$rootPath . '/application/config';
$paths = $paths .PATH_SEPARATOR.$rootPath . '/library';
set_include_path($paths);

require "Bootstrap.php";

Bootstrap::setBaseDir($rootPath);

Bootstrap::main();