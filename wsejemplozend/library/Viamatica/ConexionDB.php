<?php

//error_reporting(0);

/**
 * Permite conectarnos a la base de datos
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage ConexionDB
 */


class Viamatica_ConexionDB
{
   /**
    *
    * @var Zend_Db_Adapter_Abstract
    */
   private $conexion = null; 
   
   
   /**
    * @return Zend_Db_Adapter_Abstract
    */
   public function getConexion(){
       return $this->conexion;
   }
   
   /**
    * Establece la conexion con la base de datos
    * 
    * @return boolean
    */ 
   public function conectar()
    {
      //  try{
            $config = Zend_Registry::get("config");
            
            $this->conexion = Zend_Db::factory($config->database);
            unset ($config); //libera
           // $cx             = $this->conexion->getConnection();  //Se prueba la conexion
            Zend_Db_Table::setDefaultAdapter($this->conexion);

            return true;

 /*       }catch (Exception $e) {
            unset($db); //libera
            throw new Exception($e->getMessage());
            return false;
        }//end try*/
    }//end function Conectar

    
    public function desconectar(){
        try{        
            if ($this->conexion->isConnected()){
                $this->conexion->closeConnection();
                return true;
            }else{
                return true;                
            }//end if

        }catch (Zend_Db_Exception $ex) {
            $obj_ex = new Viamatica_Exception($ex, Viamatica_Log::ALERT);
            
            return false;
        }//end fi
    }//end function desconectar
    
}//end class