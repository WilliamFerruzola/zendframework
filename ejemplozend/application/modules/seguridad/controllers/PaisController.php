<?php

require_once("seguridad/models/PaisModelo.php");

class seguridad_PaisController extends Zend_Controller_Action{
    private $_config;
    

/*---------------------------------------------------------------------------------------*/
    public function init(){
/*---------------------------------------------------------------------------------------*/	
        $this->_config = Zend_Registry::get('config');      
        //$this->view->baseUrl = $this->getRequest()->getBaseUrl();      
/*---------------------------------------------------------------------------------------*/		
    }//end function init()
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
    public function indexAction(){
/*---------------------------------------------------------------------------------------*/
 /*---------------------------------------------------------------------------------------*/		
    }//end function
/*---------------------------------------------------------------------------------------*/	
	
    public function cargarcomboAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();        


        $objPais         = new Pais();
        $objPaisModelo   = new PaisModelo();

        $dato = $objPaisModelo->obtenerCombo();
        echo $dato;

    }//end public function cargarcomboAction(){
    
}//end class IndexController

