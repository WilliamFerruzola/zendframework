<?php

class Zend_View_Helper_FibsysMenuModulos extends Zend_View_Helper_Abstract{

    /**
     * Obtiene el Menú de los Módulos parecido al botón de inicio de Windows
     *
     * @return String  Devolver el menú en codificacion HTML el mismo que deberá ser visualizado por la vista por medio de un ECHO
     */

    public function FibsysMenuModulos()
    {
        return Viamatica_MenuModulos::generaHtml();
    }//end function ViamaticaMenuIzquierdoPopup()

}//Zend_View_Helper_FibsysMenuModulos
    //-------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------//


?>