/*-------------------------------------------------------------------------------------------------------------------------------------*/
//Se realiza el submit del formulario a traves de ajax
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function submit_formulario(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    //Se llama al loader
    ViamaticaMensajeCargandoVisible(true);
    
    //Se desactivan las opciones del MenuBur y MenuContext
    MenuBar_EnableButtons(false);
    MenuContext_EnableButtons(false); 

    $("#frm :input").attr('disabled',false); 
    
    $.ajax({
            type: 'POST',
            url: $("#frm").attr('action'),
            data:$("#frm").serialize(),
            //dataType:'json',
            // Mostramos un mensaje con la respuesta de PHP
            success: function(data) {
                    MenuBar_EnableButtons(true);                    
                    MenuContext_EnableButtons(true); 

                    accion_anterior = $('#accion').val();
                    
                    $("#div_formulario").empty();    
                    $('#div_formulario').html(data);                    

                    /*----Si la accion ejecutada fue ingresar, entonces se carga las opciones del menu del --------*/
                    /*----con las opciones que le corresponde para el codigo de cargogenerado por el ingreso ---*/
                    if (accion_anterior=='I'){
                        cargarFormularioMenuIzquierdo(baseUrl+'/seguridad/cargo/cargarmenuizquierdo/cod_cargo/'+$('#cod_cargo').val()+'/flagMenuIzq/S');
                    }//end if
                    /*---------------------------------------------------------------------------------------------*/
                    
                    setComportamientoFormulario();
                    //Se oculta el loader
                    ViamaticaMensajeCargandoVisible(false);
                    //-----------------------------------------------------------------------------------------
            }
    })
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function 
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*
 *Cuando se realiza un cambio en uno de los campos del formulario se habilitar el 
 *boton grabar tanto en el MenuBar como en el MenuContextual
 **/
/*-------------------------------------------------------------------------------------------------------------------------------------*/
function HabilitarBotonGrabar(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/    //Menu Superior
    MenuBar_EnableButton('principal','opciones','grabar',true);
    MenuContext_EnableButton('MenuContextual_Grabar', true);    
    $('#flagcambio').val('S');
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function HabilitarBotonGrabar
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Grabar_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    $('#frm').attr("action", baseUrl+'/seguridad/cargo/grabar');
    
    if ($("#frm").validationEngine('validate')){
        submit_formulario();    
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Grabar_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Eliminar_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    jConfirm('Estas Seguro de Eliminar?','FibSys', function(r){
                                                    if(r){
                                                        $('#accion').attr('value','E');
                                                        $('#frm').attr("action", baseUrl+'/seguridad/cargo/grabar');

                                                        submit_formulario();    
                                                    }else{
                                                        return false;
                                                    }
    });//End Confirm

/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Grabar_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Salir_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    if ($('#flagcambio').val()=='S'){
        jConfirm('Estas Seguro de Salir? !Tiene cambios que no ha grabado!','FibSys', function(r){
                                                    if(r){
                                                            window.close();	
                                                    }else{
                                                        return false;
                                                    }
        });//End Confirm
    }else{
       window.close(); 
    }
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Salir_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------------------------------------------------------*/
function MenuBar_Nuevo_OnClick(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    $('#accion').attr('value','I');
    setComportamientoFormulario();
/*-------------------------------------------------------------------------------------------------------------------------------------*/
}//end function MenuBar_Nuevo_OnClick
/*-------------------------------------------------------------------------------------------------------------------------------------*/

function setComportamientoFormulario(){
    switch($('#accion').val()){
        case 'I':  //Ingreso
            jQuery("#frm").validationEngine('hideAll');

            $('#cod_cargo').val('');
            $('#nom_cargo').val('');
            $('#est_cargo').val('A'); //Activo
            $('#flagcambio').val('');   

            $("#frm :input").attr('disabled',false);  
            $('#cod_cargo').attr('disabled',true);    
            $('#est_cargo').attr('disabled',true);    

            $('#mensaje').hide()

            MenuBar_EnableButtons(false);
            MenuContext_Inicializar();	            

            MenuBar_EnableButton('principal','opciones','nuevo',true);
            MenuBar_EnableButton('principal','opciones','grabar',true);
            MenuBar_EnableButton('principal','opciones','salir',true);
            MenuContext_EnableButton('MenuContextual_Grabar', true);
            MenuContext_EnableButton('MenuContextual_Nuevo', true);
            break;
            
        case 'M':  //Modificacion
            MenuBar_EnableButtons(true);                    
            MenuContext_EnableButtons(true); 
            MenuBar_EnableButton('principal','opciones','grabar',false);
            MenuContext_EnableButton('MenuContextual_Grabar', false);
            
            /*Pregunta si esta inactivado el registro*/
            if ($('#est_cargo').val()=='I'){
                MenuBar_EnableButton('principal','opciones','eliminar',false);
                MenuContext_EnableButton('MenuContextual_Eliminar', false);
                
                $("#frm :input").attr('disabled',true);  
                $('#est_cargo').attr('disabled',false);    
            }//end if
            break;
    }//end switch

    $('#flagcambio').val('');   
}

/*-------------------------------------------------------------------------------------------------------------------------------------*/
//Funcion Ready Jquery
/*-------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function(){
/*-------------------------------------------------------------------------------------------------------------------------------------*/
    //Anexa al manejar change
    $("#frm :input").change(HabilitarBotonGrabar);

    //Realiza el trim a todos los controles input
    inicializarTrimControles(); 
    
    // binds form submission and fields to the validation engine
    jQuery("#frm").validationEngine();

    //Menu Superior
    MenuBar_Button_EventClickUp('principal','opciones','nuevo',MenuBar_Nuevo_OnClick);
    MenuBar_Button_EventClickUp('principal','opciones','grabar',MenuBar_Grabar_OnClick);
    MenuBar_Button_EventClickUp('principal','opciones','eliminar',MenuBar_Eliminar_OnClick);
    MenuBar_Button_EventClickUp('principal','opciones','salir',MenuBar_Salir_OnClick);

    //Menu Contextual
    MenuContext_Inicializar();	
    MenuContext_Button_EventClickUp('MenuContextual_Nuevo',MenuBar_Nuevo_OnClick);    
    MenuContext_Button_EventClickUp('MenuContextual_Grabar',MenuBar_Grabar_OnClick);
    MenuContext_Button_EventClickUp('MenuContextual_Eliminar',MenuBar_Eliminar_OnClick);
    MenuContext_Button_EventClickUp('MenuContextual_Salir',MenuBar_Salir_OnClick);

    $('#cod_cargo').attr('disabled',true);
    $('#est_cargo').attr('disabled',true);    

    MenuContext_EnableButton('MenuContextual_Salir', true);    

    setComportamientoFormulario();
    $('#nom_cargo').focus();
/*-------------------------------------------------------------------------------------------------------------------------------------*/
});//End Funcion Ready Jquery
/*-------------------------------------------------------------------------------------------------------------------------------------*/
