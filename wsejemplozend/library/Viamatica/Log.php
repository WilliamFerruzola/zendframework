<?php

/**
 * Nos permitirá identificar los codigos de niveles de LOG que se puede utilizar
 * en el sistemas, de los cuales los mas utilizados serian:
 *      
 *      EMERG -> 0, ERR -> 3, WARN -> 4, DEBUG -> 7
 * 
 * Dependiendo de la necesidad del programador se usarán estas constantes
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage ConexionDB
 */


class Viamatica_Log
{
    const EMERG     = 'emerg';
    const ALERT     = 'alert';
    const CRIT      = 'crit';
    const ERR       = 'err';
    const WARN      = 'warn';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
    
}//end class