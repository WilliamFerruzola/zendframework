<?php

/**
 * Permite acceder al combo de estado
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage Icono
 */


class Viamatica_Icono 
{    
    //Utilizado por el menuBar
    const ICO_NUEVO                = 'icon_nuevo.png';
    const ICO_NUEVO_APAGADO        = 'icon_nuevo_apagado.png';
    const ICO_GRABAR               = 'icon_guardar.png';
    const ICO_GRABAR_APAGADO       = 'icon_guardar_apagado.png';
    const ICO_ELIMINAR             = 'icon_eliminar.png';
    const ICO_ELIMINAR_APAGADO     = 'icon_eliminar_apagado.png';
    const ICO_SALIR                = 'icon_salir.png';
    const ICO_SALIR_APAGADO        = 'icon_salir_apagado.png';
    const ICO_EXCEL                = 'icon_excel.png';
    const ICO_EXCEL_APAGADO        = 'icon_excel_apagado.png';
    const ICO_PDF                  = 'icon_pdf.png';
    const ICO_PDF_APAGADO          = 'icon_pdf_apagado.png';
    const ICO_XML                  = 'icon_xml.png';
    const ICO_XML_APAGADO          = 'icon_xml_apagado.png';
    const ICO_LISTADO              = 'icon_listado.png';
    const ICO_LISTADO_APAGADO      = 'icon_listado_apagado.png';
    const ICO_EDITAR               = 'icon_editar.png';
    const ICO_EDITAR_APAGADO       = 'icon_editar_apagado.png';
    
    //Utilizado por el Menu Izquierdo Lateral (Principal)
    const RUTA_ICO_MODULOS  = '/images/modulos/';
    const ICO_EMPRESA       = 'icon_empresa.png';
    const ICO_SUCURSAL      = 'icon_sucursal.png';
    const ICO_USUARIO       = 'icon_usuario.png';
    const ICO_PERFILES      = 'icon_perfiles.png';
    const ICO_ASIGNACION    = 'icon_asignacion.png';
    const ICO_PERMISOS      = 'icon_permisos.png';
    const ICO_PARAMETROS    = 'icon_parametros.png';
    const ICO_LOG_ACCESOS   = 'icon_log_accesos.png';
    const ICO_LOG_SISTEMA   = 'icon_empresa.png';
    const ICO_DOCUMENTOS    = 'icon_documentos.png';
    const ICO_SERVIDORES    = 'icon_servidor.png';
    const ICO_FORMATOS      = 'icon_definicionformatos.png';
    const ICO_PLANCUENTA    = 'icon_plancuentas.png';
    
}//end class
