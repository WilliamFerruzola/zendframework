<?php

require 'wsclientes/models/dao/PersonaDAO.php';
require 'wsclientes/models/sr/PersonaRespIdSR.php';
require 'wsclientes/models/sr/PersonaRespListadoSR.php';
require 'wsclientes/models/Persona.php';



class PersonaBO extends Viamatica_BaseBO
{

    /**
     * Obtiene el grid de datos de persona
     *
     * @param string  $campo_orden          Campo Clave para realizar la Busqueda
     * @param string  $modo_orden           Campo Ordenamiento
     * @param int     $nro_pagina           Pagina donde debe de iniciar el pagineo
     * @param int     $nroreg_x_pagina      Establece el número de registros que debe traer cada pagina
     * @param int     $cod_persona          Busqueda - codigo de cod_persona
     * @param string  $nom_persona          Busqueda - Nombre de nom_persona
     * @return PersonaRespListadoSR
     */
    public function listado($campo_orden,
                            $modo_orden,
                            $nro_pagina,
                            $nroreg_x_pagina,
                            $cod_persona,
                            $nom_persona
                            )
    {
        try
        {
            $this->ConexionDB = new Viamatica_ConexionDB(); 
            $this->ConexionDB->conectar();
            
            /*------Se aplican los filtros a los parametros de entrada----*/
            $filter = new Viamatica_Filter_HTMLPurificador();
            
            $campo_orden                = $filter->filter($campo_orden);
            $modo_orden                 = $filter->filter($modo_orden);
            $nro_pagina                 = $filter->filter($nro_pagina);
            $nroreg_x_pagina            = $filter->filter($nroreg_x_pagina);
            
            $cod_persona                = $filter->filter($cod_persona);
            $nom_persona                = $filter->filter($nom_persona);
           

            unset($filter); //libera
            /*------------------------------------------------------------*/

            $PersonaDAO                 = new PersonaDAO();
            $PersonaRespListadoSR       = new PersonaRespListadoSR();

           
            
            $data = $PersonaDAO->listado($campo_orden, $modo_orden, $nro_pagina, $nroreg_x_pagina, 
                                         $cod_persona, $nom_persona
                                        );
            
      
            if ($data){
                foreach($data as $reg){
                    $PersonaSR                               = new PersonaSR();
                    $PersonaSR->cod_persona                  = $reg['cod_persona'];
                    $PersonaSR->nom_persona                  = $reg['nom_persona'];
                    $PersonaSR->ape_persona                  = $reg['ape_persona'];   
                    $PersonaSR->edad                         = $reg['edad'];   
                    $PersonaSR->sexo                         = $reg['sexo'];   
                    $PersonaSR->nom_pais                     = $reg['nom_pais'];   
                    $PersonaSR->nom_provincia                = $reg['nom_provincia'];   
                    $PersonaSR->est_doble_nacionalidad       = $reg['est_doble_nacionalidad'];   
                    $PersonaSR->est_ecuatoriano              = $reg['est_ecuatoriano'];   
                    
                    
                    $PersonaRespListadoSR->registros[] = $PersonaSR;
                    unset($PersonaSR); //libera
                }//end foreach
                    
                $PersonaRespListadoSR->tot_registros =  $data->getTotalItemCount();
                $PersonaRespListadoSR->tot_paginas   =  $data->count();

                $PersonaRespListadoSR->error_codigo  = Viamatica_Respuesta::CODIGO_OK;
                $PersonaRespListadoSR->error_mensaje = Viamatica_Respuesta::TEXTO_OK;
            }else{
                $PersonaRespListadoSR->error_codigo  = Viamatica_Respuesta::CODIGO_NO_EXISTE;
                $PersonaRespListadoSR->error_mensaje = Viamatica_Respuesta::TEXTO_NO_EXISTE;
            }//end if

            unset($data); //libera
            unset($SegEmpresaDAO); //libera
            

         } catch (Zend_Exception $e) {
            $this->registrarException($e, $PersonaRespListadoSR);
        }//end try
        
        $this->ConexionDB->desconectar();
        
        Viamatica_UTF8::convertir($PersonaRespListadoSR);
        return $PersonaRespListadoSR;

    }//end function getGridData

    
    

}//end class
?>
