<?php


class Zend_View_Helper_MenuContext extends Zend_View_Helper_Abstract{

    //-------------------------------------------------------------------------------------------------------------//
    public function MenuContext(Viamatica_MenuContext $data)
    {
        //Ubico la ruta base utilizando el Helper de la Vista
/*        $helper_view = new Zend_View_Helper_BaseUrl();
        $BaseUrl  = $helper_view->baseUrl();

        if(!Zend_Auth::getInstance()->hasIdentity()) {
            return false;
        }//end if

        $filas = Zend_Auth::getInstance()->getIdentity()->accesos;
*/
                
        $html ='<ul id="div_Menu_Contextual" class="contextMenu">';

         foreach($data->getAcciones() as $regAccion){
             $html.='<li class="MenuContextual MenuContextual_'.$regAccion->titulo.'"><a href="#MenuContextual_'.$regAccion->titulo.'">'.$regAccion->titulo.'</a></li>';
         }//endforeach
        $html.='<li class="MenuContextual_Salir separator"><a href="#MenuContextual_Salir">Salir</a></li>';
        $html.='</ul>';

        unset($helper_view);
        unset($BaseUrl);

        return $html;
    }//end function 

}//end class

?>