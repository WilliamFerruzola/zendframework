<?php

require_once "ProvinciaClass.php";

class ProvinciaModelo extends Zend_Db_Table
{
    protected $_name    = 'tbl_provincia';
    protected $_primary = array('cod_pais','cod_provincia'); //array('bug_id', 'product_id');


    public function obtenerPorPais($cod_pais){
        $select = $this->select();
        $select->where("cod_pais = ?",$cod_pais);
        
        return $this->fetchAll($select);        
    }//end function listado

    
    public function obtenerCombo($cod_pais, $cod_provincia = 0){
        $resulset = $this->obtenerPorPais($cod_pais);

        $option = "";
        foreach($resulset as $row){
            $seleccionado = "";
            if ($cod_provincia == $row["cod_provincia"]){
                $seleccionado = "selected";
            }
            $option = $option."<option value='".$row["cod_provincia"]."' ".$seleccionado.">".$row["nom_provincia"]."</option>";
        }//end foreach
        
        return $option;
    }//end function


}//end class ProvinciaModelo
/*---------------------------------------------------------------------------------------*/	
