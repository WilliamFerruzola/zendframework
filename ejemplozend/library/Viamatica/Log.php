<?php

/**
 * Nos permitirá identificar los codigos de niveles de LOG que se puede utilizar
 * en el sistemas, de los cuales los mas utilizados serian:
 *      
 *      EMERG -> 0, ERR -> 3, WARN -> 4, DEBUG -> 7
 * 
 * Dependiendo de la necesidad del programador se usarán estas constantes
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage ConexionDB
 */


class Viamatica_Log
{
    private static $logger          = NULL;
    /**
     *
     * @var Array
     */
    protected static $arr_mensaje     = NULL;
    
    const EMERG     = 'emerg';
    const ALERT     = 'alert';
    const CRIT      = 'crit';
    const ERR       = 'err';
    const WARN      = 'warn';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
        
    
    static private function obtieneLogger(){
        //Obtiene el logger
        if (is_null(self::$logger)){
            self::$logger = Zend_Registry::get('logger');
        }//end if
    }//end function obtieneLogger
    
    static public function grabarArray($data, $nivel){
        self::$arr_mensaje = $data;
        self::obtieneLogger();

        $cadena  = str_pad('', 100, '-');
        //Viamatica_UTF8::convertir($cadena);
        self::$logger->$nivel($cadena);

        self::$logger->$nivel('(EXCEPCION DE LA CAPA DE NEGOCIO)');
        
        
        foreach($data as $reg){
            self::$logger->$nivel($reg); 
        }//end foreach
        self::$logger->$nivel('');
        
    }//end function grabarArray
    
    /**
     *
     * @param Array $data 
     */
    static private function setArrMensaje($data){
        self::$arr_mensaje = $data;
    }//end function setArrMensaje
    
    /**
     *
     * @return Array 
     */
    static public function getArrMensaje(){
        return self::$arr_mensaje;
    }
    
    static public function getArrMensaje_VarDump(){
        echo('<table>');
        echo('<tr>');
        echo('<td>');
        echo('<pre>');
        foreach(self::$arr_mensaje as $key => $reg){
            echo($key.' => '.htmlentities($reg));            
            echo('<br>');
        }//end foreach
        echo('</pre>');
        Zend_Debug::dump(self::$arr_mensaje);
        echo('</td>');
        echo('<tr>');
        echo('</table>');
    }//end function 
    
}//end class getArrMensaje_VarDump