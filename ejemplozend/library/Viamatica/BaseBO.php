<?php

/**
 * Permite acceder a metodos comunes del esquema DAO
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage BaseBO
 */


class Viamatica_BaseBO 
{
    /**
     * Si ocurre algun error, en este campo se almacenará el CODIGO DEL ERROR
     *
     * @var String
     */    
    protected $_errorCodigo    = null;  

    /**
     * Si ocurre algun error, en este campo se almacenará la DESCRIPCION DEL ERROR
     *
     * @var String
     */    
    protected $_errorMensaje   = null;    

    /**
     * Si ocurre alguna excepcion en la llamada de los webservice, aqui se almacenara la TRAZA de la Excepcion
     * 
     * @var Array 
     */
    protected $_exception      = null;
    
    /**
     * Obtiene el codigo de Error
     *
     * @return String
     */        
    public function getErrorCodigo(){
        return $this->_errorCodigo;
    }

    /**
     * Obtiene el mensaje de Error
     *
     * @return String
     */            
    public function getErrorMensaje(){
        return $this->_errorMensaje;
    }

    /**
     * Obtiene la traza de la Excepcion del WebServices
     * 
     * @return Array 
     */
    public function getException() {
        return $this->_exception;
    }
    
    public  function procesarException(&$obj){
        $this->_errorCodigo     = $obj->getErrorCodigo();
        $this->_errorMensaje    = $obj->getErrorMensaje();
        $this->_exception       = $obj->getException();
        
        if ($obj->getErrorCodigo() != Viamatica_Respuesta::CODIGO_OK){
            //Registra el LOG
            Viamatica_Log::grabarArray($obj->getException(), Viamatica_Log::ERR);
            //Forzamos que se ejecute el catch
            throw new Zend_Exception($obj->getErrorMensaje());
        }//end if
    }//end function procesarException
    
}//end class
