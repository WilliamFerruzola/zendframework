<?php

/**
 * Permite acceder a metodos comunes del esquema DAO
 * 
 * @author David Salazar 
 * @version 1.0
 * @package Viamatica
 * @subpackage BaseDAO
 */


class Viamatica_BaseDAO 
{
    /**
     * Si ocurre algun error, en este campo se almacenará el CODIGO DEL ERROR
     *
     * @var String
     */    
    protected $_errorCodigo    = null;  

    /**
     * Si ocurre algun error, en este campo se almacenará la DESCRIPCION DEL ERROR
     *
     * @var String
     */    
    protected $_errorMensaje   = null;    

    /**
     * Obtiene el codigo de Error
     *
     * @return String
     */        
    public function getErrorCodigo(){
        return $this->_errorCodigo;
    }

    /**
     * Obtiene el mensaje de Error
     *
     * @return String
     */            
    public function getErrorMensaje(){
        return $this->_errorMensaje;
    }

}//end class
