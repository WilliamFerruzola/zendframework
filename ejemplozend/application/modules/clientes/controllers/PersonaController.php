<?php

class Clientes_PersonaController extends Zend_Controller_Action{

    /**
     * Nos permite controlar que el usuario haya iniciado sesion para
     * utilizar los actions del controlador, caso contrario lo llevará
     * al login del sistema
     */
    public function preDispatch(){
        /*$auth = Zend_Auth::getInstance()->getIdentity();        
        if(!Zend_Auth::getInstance()->hasIdentity()) {
                $this->_forward('index', 'login', 'seguridad');
        }//end if*/
    }//end function preDispatch()
    
    /**
     * indexAction se redireccinará a listadoAction
     */
    public function indexAction(){
        $this->_forward('listado');
    }// end function indexAction()

    
    public function listadoAction(){
        require_once('clientes/forms/PersonaBusquedaForm.php');        

        //$this->_helper->layout->disableLayout();
        $this->view->jqgrid = Zend_Registry::get("config")->jqgrid;

        $PersonaBusquedaForm  = new PersonaBusquedaForm();
        $this->view->form = $PersonaBusquedaForm;

        $this->_helper->viewRenderer->setRender('listado');
    }//end function listadoAction()


    public function gridAction(){
      try{
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            require_once('clientes/models/bo/PersonaBO.php');
            require_once('clientes/forms/PersonaBusquedaForm.php');        

            $PersonaBO   = new PersonaBO();
            $PersonaBusquedaForm  = new PersonaBusquedaForm();

            //El formulario no es valido
            if (!$PersonaBusquedaForm->isValid($this->getRequest()->getParams())){
                $json = Zend_Json::encode('Formulario Invalido');
                echo $json;            
                return;
            }//end if

            //Obtiene la informacion filtra, purificada y refinada del GetParams
            $form_data = $PersonaBusquedaForm->getValues();

            //Variables utilizadas en el jqrid
            $sidx           = (string)$form_data['sidx'];
            $sord           = (string)$form_data['sord'];
            $pagina         = (int)$form_data['page'];
            $reg_x_pagina   = (int)$form_data['rows'];

            //Variables utilizadas en el filtro de busqueda
            $cod_persona 	= $form_data['cod_persona']; 
            $nom_persona 	= $form_data['nom_persona'];

            //Obtengo los registros de la busqueda
            $result = $PersonaBO->listado($sidx, $sord, $pagina, $reg_x_pagina,
                                            $cod_persona, $nom_persona
                                          );

            //Obtengo respuesta para el jqgrid (pagina_actual,  total_paginas, total_registros)
            $response['page']       = $pagina;
            $response['total']      = $result->getTot_pagina();
            $response['records']    = $result->getTot_registros();
            
            if (!is_null($result->getSegPersonas())){
                $i=0;
                foreach($result->getSegPersonas() as $row){
                    $Persona = new Persona();

                    $Persona = $row;

                    $response['rows'][$i]['id']=$Persona->getCod_persona(); 
                    $response['rows'][$i]['cell']=array($Persona->getCod_persona(), 
                                                        $Persona->getNom_persona(),
                                                        $Persona->getApe_persona(),
                                                        $Persona->getEdad(),
                                                        $Persona->getSexo(),
                                                        $Persona->getNom_pais(),
                                                        $Persona->getNom_provincia(),
                                                        $Persona->getEst_doble_nacionalidad_full(),
                                                        $Persona->getEst_ecuatoriano_full()
                                                        );

                    unset($Persona);
                    $i++;
                }//end foreach
            }//end if

            $json = Zend_Json::encode($response);
            echo $json;  
        
         } catch (Zend_Exception $e) {
            Viamatica_Log::getArrMensaje_VarDump(Viamatica_Log::getArrMensaje());
            $obj_ex = new Viamatica_Exception($e, Viamatica_Log::EMERG);
            
            //Solo se asigna el error de la excepcion y no la traza completa
            echo ($PersonaBO->getErrorMensaje());            
        }//end try
        
    }//end function gridAction


//    /*---------------------------------------------------------------------------------------*/
//    /*
//     * Permite inicializar el menu de cargo la variable $this->view->menuIzquierdoPopup
//     * es utilizada en el Layout_Popup
//     * Aqui se aplica reglas de seguridad por permiso del usuario para saber si da permisos
//     * al usuario que navegue en las opciones
//     */
//    public function cargarmenuizquierdoAction(){            
//    /*---------------------------------------------------------------------------------------*/
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();
//
//        $baseUrl = $this->getRequest()->getBaseURL();
//        $cod_cargo = (integer)$this->getRequest()->getParam('cod_cargo');
//        $flagMenuIzq = (string)$this->getRequest()->getParam('flagMenuIzq');
//
//        if ($flagMenuIzq!=''){
//            $urlMenuIzquierdo = $baseUrl.'/seguridad/cargo/cargarmenuizquierdo/cod_cargo/'.$cod_cargo;
//        }else{
//            $urlMenuIzquierdo = "";                
//        }//end if
//
//
//        //Opciones del Menu Izquierdo al estilo Array Fabrica de Zend Framework
//        $menuIzquierdo =array();		
//        $menuIzquierdo[0] = array('TITULO','General','');
//        $menuIzquierdo[1] = array('OPCION','Definición Cargo',
//                                            'cargarFormularioCompleto("'.$baseUrl.'/seguridad/cargo/consultar/cod_cargo/'.$cod_cargo.'","'
//                                                                        .$baseUrl.'/seguridad/cargo/cargarmenubar","'
//                                                                        .$urlMenuIzquierdo
//                                                                        .'")');
//        
//    
//
//        $html = Viamatica_MenuIzquierdoOpcionesPopup::generaHtml($menuIzquierdo);
//
//        unset($menuIzquierdo); //libera
//        unset($cod_cargo);  //libera            
//
//        echo($html);            
//    }//end function inicilializaPopupCargo
//
//
//    
//    /**
//     * Crea el MenuBar para el listado
//     */
//    public function cargarmenubarprincipalAction(){
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();
//
//        $MenuBarSuperior = new Viamatica_MenuBarSuperior();
//        $MenuBarSuperior->addPestana('principal', 'Principal', true);
//
//        $MenuBarSuperior->addPanel('principal', 'opciones', 'Opciones');
//        $MenuBarSuperior->addBoton('principal', 'opciones', 'nuevo', 'Nuevo', Viamatica_MenuBarSuperior::ICO_NUEVO, Viamatica_MenuBarSuperior::ICO_NUEVO_APAGADO);
//
//        $MenuBarSuperior->addPanel('principal', 'exportar', 'Exportar');
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'excel', 'Excel', Viamatica_MenuBarSuperior::ICO_EXCEL, Viamatica_MenuBarSuperior::ICO_EXCEL_APAGADO);            
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'pdf', 'PDF', Viamatica_MenuBarSuperior::ICO_PDF, Viamatica_MenuBarSuperior::ICO_PDF_APAGADO);            
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'xml', 'XML', Viamatica_MenuBarSuperior::ICO_XML, Viamatica_MenuBarSuperior::ICO_XML_APAGADO);            
//
//        $html = $MenuBarSuperior->getMenuBar($MenuBarSuperior);
//
//        unset($MenuBarSuperior); //libera
//        echo($html);
//    }//end function cargarmenubarPrincipalAction()
//
//
//    public function cargarmenubarAction(){
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();
//
//        //Opciones del MenuBar Superior al estilo .INI Fabrica de Zend Framework pero contenida en un array
//        $MenuBarSuperior = new Viamatica_MenuBarSuperior();
//        $MenuBarSuperior->addPestana('principal', 'Principal', true);
//
//        $MenuBarSuperior->addPanel('principal', 'opciones', 'Opciones');
//        $MenuBarSuperior->addBoton('principal', 'opciones', 'nuevo', 'Nuevo', Viamatica_MenuBarSuperior::ICO_NUEVO, Viamatica_MenuBarSuperior::ICO_NUEVO_APAGADO);
//        $MenuBarSuperior->addBoton('principal', 'opciones', 'grabar', 'Grabar', Viamatica_MenuBarSuperior::ICO_GRABAR, Viamatica_MenuBarSuperior::ICO_GRABAR_APAGADO);
//        $MenuBarSuperior->addBoton('principal', 'opciones', 'eliminar', 'Eliminar', Viamatica_MenuBarSuperior::ICO_ELIMINAR, Viamatica_MenuBarSuperior::ICO_ELIMINAR_APAGADO);
//        $MenuBarSuperior->addBoton('principal', 'opciones', 'salir', 'Salir', Viamatica_MenuBarSuperior::ICO_SALIR, Viamatica_MenuBarSuperior::ICO_SALIR_APAGADO);
//
//        $MenuBarSuperior->addPanel('principal', 'exportar', 'Exportar');
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'excel', 'Excel', Viamatica_MenuBarSuperior::ICO_EXCEL, Viamatica_MenuBarSuperior::ICO_EXCEL_APAGADO);            
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'pdf', 'PDF', Viamatica_MenuBarSuperior::ICO_PDF, Viamatica_MenuBarSuperior::ICO_PDF_APAGADO);            
//        $MenuBarSuperior->addBoton('principal', 'exportar', 'xml', 'XML', Viamatica_MenuBarSuperior::ICO_XML, Viamatica_MenuBarSuperior::ICO_XML_APAGADO);            
//
//        $MenuBarSuperior->addPanel('principal', 'documentos', 'Documentos');
//        $MenuBarSuperior->addBoton('principal', 'documentos', 'nuevo', 'Nuevo', Viamatica_MenuBarSuperior::ICO_NUEVO, Viamatica_MenuBarSuperior::ICO_NUEVO_APAGADO);            
//        $MenuBarSuperior->addBoton('principal', 'documentos', 'listado', 'Listado', Viamatica_MenuBarSuperior::ICO_LISTADO, Viamatica_MenuBarSuperior::ICO_LISTADO_APAGADO);            
//
//
//        $html = $MenuBarSuperior->getMenuBar($MenuBarSuperior);
//        //$html = new Viamatica_View_Helper_MenuBarSuperior($MenuBarSuperior);
//
//        unset($MenuBarSuperior); //libera
//        echo($html);
//    }//end function cargamenubarAction()
//
//    /*---------------------------------------------------------------------------------------*/
//    /* Action para levantar el popup principal de la Cargo */
//    /*---------------------------------------------------------------------------------------*/
//    public function abrirventanaAction(){
//        $config = Zend_Registry::get('config');	             
//        $this->_helper->layout()->setLayout($config->propertiesOtherLayout->layoutPopups);
//        unset($config); //libera            
//
//        $cod_cargo = (integer)$this->getRequest()->getParam('cod_cargo');
//        $accion = (integer)$this->getRequest()->getParam('accion');
//
//        //Esta inicilializacion del menu contextual y menuBar es utilizada en el Layout de Popups
//        //$this->inicilializarMenuIzquierdo($cod_cargo);  
//        //
//        //$this->inicializarMenuBar();
//        $this->view->accion = $accion;
//        $this->view->url_formulario     = $this->_request->getBaseUrl().'/seguridad/cargo/consultar/cod_cargo/'.$cod_cargo;
//        $this->view->url_menubar        = $this->_request->getBaseUrl().'/seguridad/cargo/cargarmenubar';
//        $this->view->url_menuizquierdo  = $this->_request->getBaseUrl().'/seguridad/cargo/cargarmenuizquierdo/cod_cargo/'.$cod_cargo;
//    /*---------------------------------------------------------------------------------------*/
//    }//end function openPopupCargoAction
//    /*---------------------------------------------------------------------------------------*/
//
//
//    /*---------------------------------------------------------------------------------------*/
//    public function consultarAction(){
//    /*---------------------------------------------------------------------------------------*/	
//    try{
//        $this->_helper->layout->disableLayout();
//
//        require_once('seguridad/forms/CargoMantenimientoForm.php');        
//        require_once('seguridad/models/bo/SegCargoBO.php');    
//
//        $CargoMantenimientoForm  = new CargoMantenimientoForm();
//
//        //Consulta el registro de la cargo
//        $cod_cargo = (integer)$this->getRequest()->getParam('cod_cargo');
//
//        if ($cod_cargo == 0){
//            //Obtiene el formulario
//            $dataForm = Array('cod_cargo'  => '',
//                              'nom_cargo'  => '',
//                              'est_cargo'  => 'A',   //Activo
//                              'accion'     => 'I'    //Ingreso
//                          );
//        }else{
//            $SegCargoBO              = new SegCargoBO();
//
//            $SegCargo = $SegCargoBO->obtenerPorId($cod_cargo);
//            
//            //Obtiene el formulario
//            $dataForm = Array('cod_cargo' => $SegCargo->getCod_cargo(),
//                              'nom_cargo' => $SegCargo->getNom_cargo(),
//                              'est_cargo' => $SegCargo->getEst_cargo(),
//                              'accion'    =>  'M'  //Modificacion
//                          );
//            unset($SegCargo);
//            unset($SegCargoBO);
//        }//end if
//
//        $CargoMantenimientoForm->populate($dataForm);            
//        unset($dataForm); //libera
//
//        $this->view->form = $CargoMantenimientoForm;
//
//        unset($cod_cargo);
//        
//     } catch (Zend_Exception $e) {
//        Viamatica_Log::getArrMensaje_VarDump(Viamatica_Log::getArrMensaje());
//        $obj_ex = new Viamatica_Exception($e, Viamatica_Log::EMERG);            
//
//        //Solo se asigna el error de la excepcion y no la traza completa
//        $this->view->exception    = $SegCargoBO->getErrorMensaje();            
//    }//end try
//    
//    $this->_helper->viewRenderer->setRender('mantenimiento');
//    /*---------------------------------------------------------------------------------------*/
//}// end function consultarAction()
//    /*---------------------------------------------------------------------------------------*/
////
////
//    /*---------------------------------------------------------------------------------------*/
//    public function grabarAction(){                        
//    /*---------------------------------------------------------------------------------------*/
//        $this->view->mensajeEjecucion   = null; 
//        try{
//            require_once('seguridad/forms/CargoMantenimientoForm.php');        
//            require_once('seguridad/models/bo/SegCargoBO.php');    
//
//            $this->_helper->layout->disableLayout();
//            $this->_helper->viewRenderer->setRender('mantenimiento');            
//
//            $CargoMantenimientoForm   = new CargoMantenimientoForm();
//            $SegCargoBO               = new SegCargoBO();
//            $SegCargo                 = new SegCargo();
//            $config                   = Zend_Registry::get('config');	
//
//            //El formulario no es valido
//            if (!$CargoMantenimientoForm->isValid($_POST)){
//                $this->view->mensajeIndicador = Viamatica_View_Helper_MensajeValidacion::INDICADOR_ERROR;            
//                $this->view->form = $CargoMantenimientoForm;                
//                return;
//            }//end if
//
//            $formData = $CargoMantenimientoForm->getValues();            
//
//            $SegCargo->setCod_cargo($formData['cod_cargo']);
//            $SegCargo->setNom_cargo($formData['nom_cargo']);
//            $SegCargo->setEst_cargo($formData['est_cargo']);
//
//            //Se llama al metodo mantenimiento el cual regresa el registro con que se ingresó o modificó
//            $SegCargoResult =  $SegCargoBO->mantenimiento($formData['accion'],
//                                                              $SegCargo
//                                                              );
//            unset($formData); //libera
//
//            //Posterior a esto se pregunta si la operación de mantenimiento se realizó correctamente
//            if ($SegCargoBO->getErrorCodigo()=='00')            
//            {
//                //Se consulta el registro para mostrarlo
//                $formData = Array('cod_cargo'   => $SegCargoResult->getCod_cargo(),
//                                  'nom_cargo'   => $SegCargoResult->getNom_cargo(),
//                                  'est_cargo'   => $SegCargoResult->getEst_cargo(),
//                                  'accion'      =>  'M'
//                              );
//
//                $this->view->mensajeClass = Viamatica_Mensaje::ESTILO_CLASS_MENSAJE_OK;
//                $this->view->mensajeTexto = Viamatica_Mensaje::getMensaje_ok_general();
//            }else{
//                $this->view->mensajeClass = Viamatica_Mensaje::ESTILO_CLASS_MENSAJE_ERROR;
//                $this->view->mensajeTexto = $SegPerfilBO->getErrorMensaje();
//            }//end if
//
//            $CargoMantenimientoForm->populate($formData);            
//
//            unset($formData);//Libera
//            unset($SegCargo);//Libera
//            unset($SegCargo_result); //Libera
//
//             } catch (Zend_Exception $e) {
//            //Este Log es llenado desde el BO y registrado
//            Viamatica_Log::getArrMensaje_VarDump(Viamatica_Log::getArrMensaje());
//            
//            $obj_ex = new Viamatica_Exception($e, Viamatica_Log::EMERG);    
//
//            //Solo se asigna el error de la excepcion y no la traza completa
//            $this->view->exception    = $SegPerfilBO->getErrorMensaje();
//        }//end try
//            
//       $this->view->form = $CargoMantenimientoForm;
//    /*---------------------------------------------------------------------------------------*/
//    }//end function grabarAction
//    /*---------------------------------------------------------------------------------------*/


}//end class Seguridad_CargoController
?>