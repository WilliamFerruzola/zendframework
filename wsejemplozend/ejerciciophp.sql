/*
MySQL Data Transfer
Source Host: localhost
Source Database: ejerciciophp
Target Host: localhost
Target Database: ejerciciophp
Date: 08/09/2011 20:07:07
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for tbl_pais
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pais`;
CREATE TABLE `tbl_pais` (
  `cod_pais` int(11) NOT NULL DEFAULT '0',
  `nom_pais` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cod_pais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_persona
-- ----------------------------
DROP TABLE IF EXISTS `tbl_persona`;
CREATE TABLE `tbl_persona` (
  `cod_persona` int(6) NOT NULL AUTO_INCREMENT,
  `nom_persona` varchar(50) DEFAULT NULL,
  `ape_persona` varchar(50) DEFAULT NULL,
  `edad` decimal(3,0) DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `cod_pais` decimal(2,0) DEFAULT NULL,
  `cod_provincia` decimal(2,0) DEFAULT NULL,
  `est_ecuatoriano` varchar(1) DEFAULT NULL,
  `est_doble_nacionalidad` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_persona`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_provincia
-- ----------------------------
DROP TABLE IF EXISTS `tbl_provincia`;
CREATE TABLE `tbl_provincia` (
  `cod_pais` int(11) NOT NULL DEFAULT '0',
  `cod_provincia` int(11) NOT NULL DEFAULT '0',
  `nom_provincia` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`cod_pais`,`cod_provincia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_usuario
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usuario`;
CREATE TABLE `tbl_usuario` (
  `cod_usuario` int(11) NOT NULL DEFAULT '0',
  `usr_name` varchar(30) DEFAULT NULL,
  `usr_pass` varchar(30) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `cargo` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `tbl_pais` (`cod_pais`, `nom_pais`) VALUES ('1', 'Ecuador');
INSERT INTO `tbl_pais` (`cod_pais`, `nom_pais`) VALUES ('2', 'Colombia');
INSERT INTO `tbl_pais` (`cod_pais`, `nom_pais`) VALUES ('3', 'Venezuela');
INSERT INTO `tbl_pais` (`cod_pais`, `nom_pais`) VALUES ('4', 'España');
INSERT INTO `tbl_pais` (`cod_pais`, `nom_pais`) VALUES ('5', 'Canada');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('0', '', '', '71', 'M', '1', '3', null, null);
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('1', 'Moroni Davidx', 'Salazar', '32', 'M', '1', '2', 'S', null);
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('2', 'Vanessa', 'Matamoros', '27', 'F', '1', '1', 'S', 'N');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('3', 'Castro', 'Edison', '32', 'M', '2', '3', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('4', 'Ernesto', 'Banderas', '22', 'M', '1', '1', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('5', 'Antonio', 'Escudos', '23', 'M', '2', '2', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('6', 'Nicolas', 'Lapenti', '36', 'M', '1', '3', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('7', 'Martin', 'Lutero', '56', 'M', '1', '1', null, 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('8', 'Teofilo', 'Sanchez', '45', 'M', '1', '2', 'S', null);
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('9', 'David', 'Salazar', '34', 'M', '2', '3', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('10', 'David X', 'Salazar', '33', 'M', '2', '1', 'S', null);
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('11', 'Dario', 'Monchito', '18', 'F', '2', '2', 'S', 'S');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('12', 'Eduardo ', 'Fajardo', '15', 'F', '1', '3', 's', 's');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('13', 'JOSE', 'De Alulema', '22', 'F', '1', '2', 'S', 'N');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('14', 'Hugo', 'Lopez', '11', 'F', '2', '2', 'S', 'N');
INSERT INTO `tbl_persona` (`cod_persona`, `nom_persona`, `ape_persona`, `edad`, `sexo`, `cod_pais`, `cod_provincia`, `est_ecuatoriano`, `est_doble_nacionalidad`) VALUES ('15', 'Don Ramón', 'ChiChave', '50', 'M', '1', '1', 'N', 'N');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '1', 'Guayas');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '2', 'Pichincha');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '3', 'Esmeraldas');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '4', 'El Oro');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '5', 'Manabi');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '6', 'Galapagos');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '7', 'Azuay');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('1', '8', 'Sta Elena');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('2', '1', 'Pacuta');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('2', '2', 'Pedernales');
INSERT INTO `tbl_provincia` (`cod_pais`, `cod_provincia`, `nom_provincia`) VALUES ('2', '3', 'Cartagena');
INSERT INTO `tbl_usuario` (`cod_usuario`, `usr_name`, `usr_pass`, `nombre`, `cargo`) VALUES ('1', 'dsalazar', '123456', 'David Salazar', 'Consultor');
INSERT INTO `tbl_usuario` (`cod_usuario`, `usr_name`, `usr_pass`, `nombre`, `cargo`) VALUES ('2', 'jpueblo', '1234', 'Juan Pueblo', 'Desarrollador');
