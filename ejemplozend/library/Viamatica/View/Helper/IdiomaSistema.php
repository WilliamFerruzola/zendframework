<?php

class Viamatica_View_Helper_IdiomaSistema
{

   
     /**
     * idiomaSistema
     *
     * @return String
     */    
    public function idiomaSistema()
    {
        $locale = Zend_Registry::get('Zend_Locale');
        return $locale->getLanguage();        
      }
    
}
