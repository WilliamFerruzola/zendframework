<?php

require_once("seguridad/models/ProvinciaModelo.php");


class seguridad_ProvinciaController extends Zend_Controller_Action{
    private $_config;
    

/*---------------------------------------------------------------------------------------*/
    public function init(){
/*---------------------------------------------------------------------------------------*/	
        $this->_config = Zend_Registry::get('config');      
        //$this->view->baseUrl = $this->getRequest()->getBaseUrl();      
/*---------------------------------------------------------------------------------------*/		
    }//end function init()
/*---------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------*/
    public function indexAction(){
/*---------------------------------------------------------------------------------------*/
 /*---------------------------------------------------------------------------------------*/		
    }//end function
/*---------------------------------------------------------------------------------------*/	
	
    public function cargarcomboAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();        

        if (empty($_POST['cod_pais'])) {
            $return['error'] = true;
            $return['msg'] = 'Seleccione un Pais';
        }else{
            $return['error'] = false;
            $return['msg'] = 'Guayas, Pichincha, Esmeraldas';
            $return['data'] = array('1'=>'Guayas','2'=>'Pichincha','3'=>'Esmeraldas','4'=>'El Oro');
        }
        echo json_encode($return);        
    }

    
    public function cargarcombo2Action(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();        

        if (empty($_POST['cod_pais'])) {
            $return['error'] = true;
            $return['msg'] = 'Seleccione un Pais';
        }else{
            $return['error'] = false;
            $return['msg'] = 'Guayas, Pichincha, Esmeraldas';
            $return['options'] = "<option value='1'>Guayas</option><option value='2'>Pichincha</option>"; 
            //$return['data'] = array('1'=>'Guayas','2'=>'Pichincha','3'=>'Esmeraldas','4'=>'El Oro');
        }
        echo json_encode($return);        
    }

        public function cargarcombo3Action(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();        

        $objProvinciaModelo    = new ProvinciaModelo();

        $cod_pais        = $this->_request->getParam('cod_pais');
        $dato = $objProvinciaModelo->obtenerCombo($cod_pais);

        $return['options'] = $dato;
        echo json_encode($return);        
    }

}//end class IndexController

