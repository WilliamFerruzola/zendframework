<?php

class CargoMantenimientoForm extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        
        $BaseUrl = new Zend_View_Helper_BaseUrl();
        
        $this->setName('frm')
             ->setAction($BaseUrl->baseUrl().'/default/index/index')
             ->setMethod('post')
             ->setAttrib('id', 'frm');      
        unset($BaseUrl);//libera
        
        $cod_cargo = new Zend_Form_Element_Text('cod_cargo');
        $cod_cargo->setLabel('Código')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->addFilter(new Viamatica_Filter_HTMLPurificador)                                
                    ->setAttrib('size', 5)
                    ->setAttrib('maxlength', 5);

        $nom_cargo = new Zend_Form_Element_Text('nom_cargo');
        $nom_cargo->setLabel('Nombre')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador)                                                
              ->setRequired(true)
              ->addValidator('NotEmpty',false,array('messages'=>'Ingrese el nombre del cargo'))
              ->setAttrib('size', 40)
              ->setAttrib('maxlength', 100)
              ->setAttrib('class','validate[required]');

        //Se llena el combo de Estado
        $arrEstado = Viamatica_Estado::obtenerCombo();
        $est_cargo = new Zend_Form_Element_Select('est_cargo');
        $est_cargo->setLabel('Estado')
                    ->addMultiOptions($arrEstado)
                    ->setRequired(true)
                    ->addValidator('NotEmpty',false,array('messages'=>'Seleccione un cargo'))
                    ->setValue('')
                    ->setAttrib('class','validate[required]');
        unset($arrEstado);
        
        //Campo para controlar si ha habido un cambio en el formulario
        $flagcambio = new Zend_Form_Element_Hidden('flagcambio');
        $flagcambio->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador)                                                
              ->setAttrib('size', 3);
        
        $accion = new Zend_Form_Element_Hidden('accion');
        $accion->setLabel('accion')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador)                                                
              ->setAttrib('size', 3);
        
        $submit = new Zend_Form_Element_Submit('enviar');
        $submit->setLabel('enviar')
               ->setValue('enviar');
        
        $this->addElements(array($cod_cargo, $nom_cargo, $est_cargo, $accion, $flagcambio, $submit));

        $this->setElementDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));
    }//end function init

}//end class 

?>
