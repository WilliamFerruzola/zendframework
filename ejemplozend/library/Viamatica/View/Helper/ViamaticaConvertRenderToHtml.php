<?php

/**
 * Muestra el mensaje de Cargando esto es muy util para
 * poder interactuar con los Ajax
 *
 * Debe de utilizar el javascript que se encuentra en public/js/viamatica/mensaje_cargando.js
 *
 * @author David Salazar
 * @version 1.0
 * @package Zend_View_Helper
 * @subpackage ViamaticaConvertRenderToHtml
 */

class Zend_View_Helper_ViamaticaConvertRenderToHtml extends Zend_View_Helper_Abstract{

    /**
     * Permite convertir un instancia de renderViewHelper purificar el HTML para que no
     * tenga un volcado de error en los Javascript
     * 
     * @param string $render
     * @return string 
     */
    public function ViamaticaConvertRenderToHtml($render)
    {
        $render = nl2br($render); //add html line returns
        //$render = htmlentities($render); //make remaining items html entries.        
        $render = str_replace(chr(10), "", $render); //remove carriage returns
        $render = str_replace(chr(13), "", $render); //remove carriage returns    
        $render = str_replace('<br />', "", $render); //remove carriage returns    

        return $render;
    }//end function ViamaticaConvertHtmlToJavascript()

}//end class

?>