<?php

class PersonaBusquedaForm extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        
        $BaseUrl = new Zend_View_Helper_BaseUrl();
        
        $this->setName('frm')
             ->setAction($BaseUrl->baseUrl().'/default/index/index')
             ->setMethod('post')
             ->setAttrib('id', 'frm');      
        unset($BaseUrl);//libera
        
        $cod_persona = new Zend_Form_Element_Text('cod_persona');
        $cod_persona->setLabel('Código')
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->addFilter(new Viamatica_Filter_HTMLPurificador)                                
                    ->setAttrib('size', 20)
                    ->setAttrib('maxlength', 5);

        $nom_persona = new Zend_Form_Element_Text('nom_persona');
        $nom_persona->setLabel('Nombre')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador)                                                
              ->setAttrib('size', 20)
              ->setAttrib('maxlength', 100);
        
        /*-----------CONTROLES UTILIZADOS POR EL JQGRID------------*/
        $sidx = new Zend_Form_Element_Text('sidx');
        $sidx->setLabel('sidx')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador);

        $sord = new Zend_Form_Element_Text('sord');
        $sord->setLabel('sord')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador);

        $page = new Zend_Form_Element_Text('page');
        $page->setLabel('page')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador);

        $rows = new Zend_Form_Element_Text('rows');
        $rows->setLabel('rows')
              ->addFilter('StripTags')                
              ->addFilter('StringTrim')
              ->addFilter(new Viamatica_Filter_HTMLPurificador);
        /*----------------------------------------------------------*/
        
        
        $submit = new Zend_Form_Element_Submit('buscar');
        $submit->setLabel('Buscar')
               ->setValue('Buscar');
        
        $this->addElements(array($cod_persona, $nom_persona, $sidx, $sord, $page, $rows, $submit));

        $this->setElementDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));
    }//end function init

}//end class 

?>
