<?php

class PersonaSR{
     /** @var integer */
    public $cod_persona;
    /** @var String */
    public $nom_persona;
    /** @var String */
    public $ape_persona;
    /** @var integer */
    public $edad;
    /** @var String */
    public $sexo;
    /** @var integer */
    public $cod_pais;
    /** @var integer */
    public $cod_provincia;
    /** @var String */
    public $est_ecuatoriano;
    /** @var String */
    public $est_doble_nacionalidad;     
    
    /** @var String */
    public $nom_pais;
    /** @var String */
    public $nom_provincia;         
}

?>