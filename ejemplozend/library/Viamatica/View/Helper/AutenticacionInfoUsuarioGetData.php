<?php

class Viamatica_View_Helper_AutenticacionInfoUsuarioGetData
{

   
     /**
     * idiomaSistema
     *
     * @return String
     */    
    public function autenticacionInfoUsuarioGetData()
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->getIdentity()->user;        
            return $auth;
        }//end if
      }
    
}
